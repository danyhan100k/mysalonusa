import { createStore } from 'redux';
import messageReducer from '../reducers/messageReducer';

const configureStore = (railsProps) => (
	// railsProps = initialState
	createStore(messageReducer, railsProps)
); //// { dispatch, getState, subscribe}

// function createStore(reducer, initialState) {

// 	let state;
// 	let listeners = [];
// 	function dispatch(action){
// 		state = reducer(state, action)
// 		listeners.forEach(listener => listener())
// 	};
// 	function getState() { return state; }
// 	function listeners(listener) {
// 		listeners.push(listener);
// 		return function() {
// 			listeners.filter(l => l != listener);
// 		};
// 	};
// 	state = reducer(initialState, {});
// 	return {
// 		dispatch,
// 		getState,
// 		listeners
// 	}
// };

export default configureStore;