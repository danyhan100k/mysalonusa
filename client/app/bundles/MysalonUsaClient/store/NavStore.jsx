import { createStore, applyMiddleware } from 'redux';
import navReducer from '../reducers/navReducer';
import createLogger from 'redux-logger';

const configureStore = (railsProps) => {
	const store = createStore(
		navReducer,
		railsProps,
		applyMiddleware(createLogger())
	);
	return store;
};


export default configureStore;