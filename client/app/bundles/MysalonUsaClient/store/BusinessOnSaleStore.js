import { createStore, applyMiddleware } from 'redux';
import businessOnSaleReducer from '../reducers/businessOnSaleReducer';
import createLogger from 'redux-logger';
import { thunk } from '../shared/shared.js';

const configureStore = (railsProps) => { //railsProps == initialState
	const store = createStore(
		businessOnSaleReducer, 
		railsProps,
		applyMiddleware(thunk, createLogger())
	);
	return store;
};

export default configureStore;