const { createStore, applyMiddleware } = require('redux');
const { rootReducer } = require('../reducers/UsedItemsReducer');
const createLogger =  require('redux-logger');
const { thunk } = require('../shared/shared.js');


const configureStore = (railsProps) => {
	const store = createStore(
		rootReducer,
		railsProps,
		applyMiddleware(thunk, createLogger())
	);
	return store;
};

export default configureStore;
