import { createStore, applyMiddleware } from 'redux';
import createBusinessOnSaleReducer from '../reducers/createBusinessOnSaleReducer';
import createLogger from 'redux-logger';
import ReduxThunk from 'redux-thunk';

const configureStore = (railsProps) => {
	const store = createStore(
		createBusinessOnSaleReducer,
		railsProps,
		applyMiddleware(ReduxThunk, createLogger())
	);
	return store;
};

export default configureStore;
