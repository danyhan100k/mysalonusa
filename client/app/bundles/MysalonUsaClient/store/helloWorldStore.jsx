import { createStore } from 'redux';
import helloWorldReducer from '../reducers/helloWorldReducer';

const configureStore = (railsProps) => (
  createStore(helloWorldReducer, railsProps) //helloWorldReducer(railsProps, action)
);

export default configureStore;
