import React from 'react';
import { connect } from 'react-redux';
import Pagination from 'react-js-pagination';
import { 
	getSuggestionValue, 
	setItems,
	loaderOn
} from '../actions/usedItemsActionCreators.jsx';
import NotFound from '../components/NotFound';
import DotLoader from '../components/DotLoader';
import TimeAgo from 'react-timeago';
import { 
	formatDate 
} from '../shared/shared.js';
import BoxLinks from '../components/boxLinks';

function openModal(item) {
	const createdAt = formatDate(
		new Date(
			item.created_at_utc.replace(/-/g, "/")
		)
	);
	let images = ["<div class='modal-images-wrapper'>"];
	item.images.forEach(img => {
		images.push(
			`<a href=${img} target='_blank'><img class='ui medium image' src=${img} style='width: 100%; margin: auto;'></a>`
		);
	});
	images.push(`</div>`);
	const modal = 
		`<div class='modal fade' id='used-item-modal'>
		  <div class='modal-dialog modal-lg' role='document'>
		    <div class='modal-content'>
		      <div class='modal-header'>
		        <h5 class='modal-title'>${item.title}</h5>
		        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
		          <span aria-hidden='true'>&times;</span>
		        </button>
		      </div>
		      <div class='modal-body'>
		        <p>${item.description}</p>
		      </div>
		      <div class='list-group'>
			        <button 
			        	type='button' 
			        	class='list-group-item'>
			        	<div class='ui label teal job-modal-label'>
			        		Price
			        	</div>
			        	${item.price}
			        </button>

			        <button 
			        	type='button' 
			        	class='list-group-item'>
			        	<div class='ui label teal job-modal-label'>
			        		Contact Email 
			        	</div>
			        	<a href='mailto:${item.email}'>
			        		email
			        	</a>
			        </button>
			        <button 
			        	type='button' 
			        	class='list-group-item'>
			        	<div class='ui label teal job-modal-label'>
			        		Location
			        	</div>
			        	${item.city_name} ${item.state_abbreviation} ${item.city_zip}
			        </button>

			        <button 
			        	type='button' 
			        	class='list-group-item'>
			        	<div class='ui label teal job-modal-label'>
			        		Created At
			        	</div>
			        	${createdAt}
			        </button>

			        <button 
			        	type='button' 
			        	class='list-group-item'>
			        	<div class='ui label teal job-modal-label'>
			        		Views
			        	</div>
			        	${item.views}
			        </button>

			        <button 
			        	type='button' 
			        	class='list-group-item'>
			        	<div class='ui label teal job-modal-label'>
			        		Phone Numbers 
			        	</div>
			        	${item.phone_numbers.join(", ")}
			        </button>
			        ${images.join('')}
			    </div>
		      <div class='modal-footer'>
		        <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>
		      </div>
		    </div>
		  </div>
		</div>`;

		return modal;
};

const ItemCard = ({ item }) => {
	const created_at = new Date(item.created_at_utc);
	let createdAtForMobile;
	if ($(window).width() <= 992) {
		createdAtForMobile = formatDate(
			new Date(
				item.created_at_utc.replace(/-/g, "/")
			)
		);
	};
	return(
		<div 
			className="col-sm-6 col-md-3 col-xs-12"
			onClick={() => {				
				$(openModal(item)).modal();
			}}
		>
			<div className="used-item ui card">
	      <div className="content">
	        <img 
	        	className="ui avatar image" 
	        	src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRu-f4KE9dD5pll0_ddkK3Owdg0QV0keE79q_yvhwjdtmE1O0-z&reload=on" 
	        />
	        { item.title }
	      </div>
	      <div className="image">
	        <img className="ui wireframe image" src="https://images.craigslist.org/00t0t_jUMBvKxFMvy_600x450.jpg" />
	        <a className="ui orange right ribbon label">{item.price}</a>
	      </div>
	      <div className="content">
	        <div className="right floated meta">
	        	{
	        		createdAtForMobile ? `Posted: ${createdAtForMobile}`
	        		:
	        		<TimeAgo 
								date={created_at.toString()} 
							/>
	        	}
	        </div>
	        <i className="heart outline like icon"></i>
	        { `${item.views} views`}

	      </div>
	      <div className="extra content">
	        <div className="ui large transparent left icon input">
	          { `${item.city_name}, ${item.state_abbreviation}`}
	        </div>
	      </div>
	    </div>
	  </div>
	);
};

const UsedItemsList = ({ 
	usedItems, 
	loader,
	pageChange 
}) => {
	// TODO: updateViewCount
	if (loader) {
		return <DotLoader open={loader} />
	};
	if (usedItems.length == 0) {
		return <NotFound msg={"California, Illinois, New York, Chair"}/>
	};
	return(
		<div className="used-items ui grid container">
			<div className="col-xs-12">
				<h2 className="ui horizontal divider header length-header">
		      <i className="tag icon"></i>
		      {`Showing ${usedItems.usedItems.length} items`}
		    </h2>
			</div>
			<div className="ui link cards">
				{
					usedItems.usedItems.map(item => {
						return(			
							<ItemCard key={item.id} item={item} />
						);
					})
				}
			</div>
			<div className="col-xs-12 pagination-container">
				<Pagination
					className="pagination"
					activePage={usedItems.currentPage}
					itemsCountPerPage={usedItems.perPage}
					totalItemsCount={usedItems.totalEntries}
					onChange={(page) => {
					 pageChange(page, usedItems.searchedValue);
					}}
				/>
				<BoxLinks
					firstLink={"Post My Used Item For Sale"}
					secondLink={"See Used Items List"}
				/>
			</div>
		</div>
	);
};

const mapStateToProps = ({ usedItems, loader }) => ({ 
	usedItems,
	loader,
});
//TODO: updateViewCount
const mapDispatchToProps = (dispatch) => ({
	pageChange(page, value) {
		const url = `/api/used_items/search.json?search_value=${value}&page=${page}`;
		dispatch(loaderOn());
		getSuggestionValue(url)
				.then(items => dispatch(setItems(items)))
				.catch(error => console.log("Request for fetching Used Items failed: ", error))
		
	}, //pageChange

});

const UsedItemsListContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(UsedItemsList);

export default UsedItemsListContainer;










