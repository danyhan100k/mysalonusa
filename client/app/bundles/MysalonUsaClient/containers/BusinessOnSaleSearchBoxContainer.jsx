import { connect } from 'react-redux';
import AutosuggestWrapper from '../components/AutosuggestWrapper';
import { 
	getSuggestionValue, 
	setSalons, 
	loaderOn,
	apiFetchSuggestions,
} from '../actions/usedItemsActionCreators';
import 'whatwg-fetch'; 

const mapStateToProps = ({ suggestions, initialSearch }) => {
	return {
		suggestions,
		initialSearch
	};
};
const mapDispatchToProps = (dispatch) => {
	return {
		fetchSuggestions(value) {
			dispatch(
				apiFetchSuggestions(`/api/business_on_sales/suggestions.json?suggestion=${value}`)
			);
		},
		getSuggestionValue(suggestion) {
			dispatch(loaderOn());
			const url = `/api/business_on_sales/salons.json?search_value=${suggestion}`;
			getSuggestionValue(url).then(salons => {
				dispatch(setSalons(salons))
			})
			.catch(error => { console.log('Request for fetching Salons failed: ', error); });
			return suggestion;
		}
	};
}
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AutosuggestWrapper)