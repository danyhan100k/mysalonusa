import React from 'react';
import { connect } from 'react-redux';
import BusinessOnSaleFormContainer from './BusinessOnSaleFormContainer';
import StateOptionsOrCreatedPost from './StateOptionsOrCreatedPost';
import SpinnerContainer from './SpinnerContainer';

const BusinessOnSaleFormRenderer = () => (
	<div>
		<SpinnerContainer />
		<StateOptionsOrCreatedPost />
		<BusinessOnSaleFormContainer />
	</div>
);

export default BusinessOnSaleFormRenderer;