import { connect } from 'react-redux';
import SalonsList from '../components/SalonsList';
import 'whatwg-fetch'; 

const mapStateToProps = ({ salons, loader }) => ({
	salons,
	loader,
});

const mapDispatchToProps = (dispatch) => ({
	updateViewCount(id) {
		fetch(`api/business_on_sales/update_views?salon_id=${id}`)
		.then(response => {
			if (response.status >= 200 && response.status < 300) {
			} else {
				let error = new Error(response.statusText);
				throw error
			};
		})
		.catch(error => {
			console.log('Error updating view count: ', error);
		});
	},
	pageChange(page, value) {
		dispatch({ type: 'LOADER_ON' });		
		fetch(`/api/business_on_sales/salons.json?search_value=${value}&page=${page}`)
		.then(response => {
			if (response.status >= 200 && response.status < 300) {
				return response.json();
			} else {
				let error = new Error(response.statusText);
				throw error
			}
		})
		.then(salons => {
			dispatch({
				type: 'SET_SALONS',
				salons
			});
		})
		.catch(error => { 
			console.log('Request for fetching Salons failed: ', error);
		})
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SalonsList);