import React from 'react';
import { connect } from 'react-redux';
import StateOptionsContainer from './StateOptionsContainer';
import CreatedBusinessOnSalePost from './CreatedBusinessOnSalePost';

const mapStateToProps = ({ createdSalon }) => ({ createdSalon });
const mapDispatchToProps = (dispatch) => ({

});

const StateOptionsOrCreatedPost = ({ createdSalon }) => { 
	if (!createdSalon.created_at) {
		return(
			<StateOptionsContainer />
		);
	} else {
		return(
			<CreatedBusinessOnSalePost 
				salon={createdSalon}
			/>
		);
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(StateOptionsOrCreatedPost);