import React from 'react';
import { connect } from 'react-redux';
import 'whatwg-fetch'; 

const mapStateToProps = () => ({});
const mapDispatchToProps = (dispatch) => ({
	onClickState(state) {
		fetch(`/api/business_on_sales/cities?state=${state}`)
		.then(response => {
			if (response.status >= 200 && response.status < 300) {
				return response.json();
			} else {
				let error = new Error(response.statusText);
				throw error
			}
		})
		.then(cities => {
			dispatch({
				type: 'ON_FETCHED_CITIES',
				cities,
				state
			});
		})
		.catch(error => {
			console.log('Request for fetching Cities failed: ', error);
		});
	} //onClickState
});

const StateOptions = ({ onClickState }) => {
	return(
		<div className="choose-city-container row">
			<div className="choose-city-menu">
				<h1 style={{ fontWeight: 100, color: '#f38034', margin: '1em 0 0.5em'}}>
					Sale My Business
				</h1>
				<h2
					style={{ margin: '0.2em 0 1em'}}
				> 
					Choose Your City 
				</h2>
				<div 
					className="city col-xs-12 col-sm-4 col-md-4"
					onClick={() => {
						onClickState("california");
					}}
				>
					<img 
						className="la-image"
						src="https://s3.us-east-2.amazonaws.com/mysalonusa/uploads/cities/california.jpg"
					/>
				</div>
				<div 
					className="city col-xs-12 col-sm-4 col-md-4"
					onClick={() => {
						onClickState("new york");
					}}
				>
					<img 
						className="ny-image"
						src="https://s3.us-east-2.amazonaws.com/mysalonusa/uploads/cities/newyork.jpg"
					/>
				</div>
				<div 
					className="city col-xs-12 col-sm-4 col-md-4"
					onClick={() => {
						onClickState("illinois");
					}}
				>
					<img 
						className="chi-image"
						src="https://s3.us-east-2.amazonaws.com/mysalonusa/uploads/cities/chicago.jpg"
					/>
				</div>
			</div>
		</div>
	);
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(StateOptions);