import React from 'react';
import TimeAgo from 'react-timeago';

const CreatedBusinessOnSalePost = ({ 
	salon 
}) => {
	const headerStyle = { textAlign: 'center', color: '#42ab9e'};
	return(
		<div className="col-xs-12">
			<h1 style={Object.assign({}, headerStyle, { margin: '1.2em 0em 0.2em' })}>
				Success! 
			</h1>
			<h2 style={Object.assign({}, headerStyle, { marginTop: '.2em'})}>
				Created Your Business On Sale!
			</h2>
			<div className="ui card" style={{ width: '700px', margin: '0 auto'}}>
				<div className="content">
					<div className="header">
						{salon.title}
					</div>
					<div className="meta">
						<TimeAgo date={new Date(salon.created_at_utc)} />
					</div>
					<div 
						className="description"
						style={{ wordBreak: 'break-all'}}
					>
						<p>{salon.description}</p>
					</div>
					<div className="ui divided selection list">
			      <a className="item">
			        <div className="ui teal horizontal label">Company Name</div>
			        { salon.company.name }
			      </a>
			      <a className="item">
			        <div className="ui teal horizontal label">Contact Email</div>
			        { salon.contact_email }
			      </a>
			      <a className="item">
			        <div className="ui teal horizontal label">Price</div>
			        { salon.price || "Private"}
			      </a>
			      <a className="item">
			        <div className="ui teal horizontal label">Creator</div>
			        { salon.company.user.email }
			      </a>

			      <div className="ui items">
			      	{
			      		salon.images.map(img => (
			      			<div 
			      				className="item"
			      				key={img.url}
			      			>
						        <div className="ui small image">
						          <img src={img.url} />
						        </div>
						      </div>
			      		))
			      	}
				    </div>

		    	</div>
				</div>

				<div className="extra content">
					<a href="/business_on_sales">
						<button 
							className="mdl-button--colored mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect"
						>
						  Search My Salon
						</button>
					</a>
				</div>
			</div>
		</div>
	);
};


export default CreatedBusinessOnSalePost;