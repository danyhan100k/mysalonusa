import React from 'react';
import { connect } from 'react-redux';
//Not creating real containers yet
const NavMenu = ({ 
  user, 
  tokens_left,
  business_post_left,
  state,
  state_id,
  language
}) => {
  if (user) {
    return(
      <div>
        <a 
          className="item"
          href='/'
          >
          <div className="ui small"></div>
          { language.home }
        </a>
        <a className="item">
          <div className="ui small"></div>
          { user }
        </a>
        <a className="item">
          <div className="ui small"></div>
          <span className="tokens">{business_post_left}</span>
          { ` ${language.business_post_left}`}
        </a>
        <a 
          className="item"
          href={`/state/${state_id}/${state}/jobs`}
        >
          <div className="ui small"></div>
          { language.search_jobs }
        </a>
        <a 
          className="item"
          href="/business_on_sales"
        >
          <div className="ui small"></div>
          { language.search_business_on_sale }
        </a>
        <a 
          className="item"
          href="/business_on_sales/new"
        >
          <div className="ui small"></div>
          { language.sale_or_post_my_business }
        </a>
         <a 
          className="item"
          href='/jobs/choose_state'
          >
          <div className="ui small"></div>
          { language.post_jobs }
        </a>
      </div>
    )
  } else {
    return(
      <div>
        <a 
          className="item"
          href='/'
          >
          <div className="ui small"></div>
          { language.home }
        </a>
        <a 
          className="item"
          href={`/state/${state_id}/${state}/jobs`}
        >
          <div className="ui small"></div>
          { language.search_jobs }
        </a>
        <a 
          className="item"
          href="/business_on_sales"
        >
          <div className="ui small"></div>
          { language.search_business_on_sale }
        </a>
        <a 
          className="item"
          href='/business_on_sales/new'
        >
          <div className="ui small"></div>
          { language.sale_or_post_my_business }
        </a>
         <a 
          className="item"
          href='/jobs/choose_state'
        >
          <div className="ui small"></div>
          { language.post_jobs }
        </a>
      </div>
    );
  }
};
const NavMenuContainer = ({ mode, content, language, toggleMode }) => {
  const {
    open
  } = mode;
  const { 
    current_user, 
    tokens_left,
    business_post_left,
    state_name,
    state_id
  } = content;

  const width = $(window).width();
  if (width <= 961) { return(<div></div>)}
  	return(
  		<div 
        className="react-nav-menu ui large vertical menu"
        onClick={() => toggleMode()}
      >
        <a className="menu item">
          <div className="ui small"></div>
          <span className="mdl-badge" data-badge="♥">
            { language.menu }
          </span>
          <span 
            style={{ display: 'block', fontSize: '0.52em', marginTop: '0.4em'}}
          >
          { 
              !open ? 
                <span>{ language.click_on }</span>
              : <span>{ language.click_off }</span>
            }
          </span>
        </a>
        {
          open ?
            <NavMenu 
              user={current_user} 
              tokens_left={tokens_left} 
              business_post_left={business_post_left}
              state={state_name}
              state_id={state_id}
              language={language}
            />
          :
          <div></div>
        }
      </div>
  	);
};

const mapStateToProps = ({ mode, content, language }) => ({
  mode,
  content,
  language
});

const mapDispatchToProps = (dispatch) => ({
  toggleMode() {
    dispatch({
      type: 'TOGGLE_MODE'
    });
  }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavMenuContainer);





