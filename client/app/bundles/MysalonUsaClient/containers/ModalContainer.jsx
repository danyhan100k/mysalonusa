import { connect } from 'react-redux';
import Modal from '../components/Modal';

const mapStateToProps = ({ modal }) => (modal);

const mapDispatchToProps = (dispatch) => ({
	onModalExit() {
		dispatch({ type: 'ON_MODAL_EXIT' });
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Modal);