import React from 'react';
import { connect } from 'react-redux';
import DropzoneWrapper from '../components/DropzoneWrapper';

const mapStateToProps = ({ salon, modal }) => ({
	salon,
	modal
});

const mapDispatchToProps = (dispatch) => ({
	onBack() {
		dispatch({
			type: 'ON_BACK'
		})
	},
	onImageUpload(images) {
		dispatch({
			type: 'ON_IMAGE_UPLOAD',
			images
		});
	},
	onRemoveImage(image) {
		dispatch({
			type: 'ON_REMOVE_IMAGE',
			image
		});
	},
	onSalonChange(value) {
		dispatch({
			type: 'ON_SALON_CHANGE',
			value
		})
	},
	onFormSubmit(salon) {
		dispatch({ type: 'SHOW_SPINNER' })
		dispatch((dispatch, getState) => {
				
				//if there are images, multipart/form-data request
				//FormData object uses the same format a form would use 

				//if the encoding type were set to "multipart/form-data".
				//multipart/form-data' means that no characters will be encoded. 
				//that is why this type is used while uploading files to server.
				const formData = new FormData();	
				formData.append("business_on_sale", JSON.stringify(getState().salon));

				if (salon.images.length) {
					salon.images.forEach(({ file }) => {
						formData.append("images[]", file);
					});
				};

				jQuery.ajax({
		        url: '/api/business_on_sales',
		        type: 'POST',
		        data: formData,
		        cache: false,
		        dataType: 'json',
		        processData: false, // Don't process the files
		        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
		        success: (data, textStatus, jqXHR) => {

		        	if (typeof data.error === 'undefined') {
		        		dispatch({
		        			type: 'SALON_CREATED',
		        			createdSalon: data.createdSalon
		        		});

		        	} else {
								console.error('Creating Business On Sale had errors: ', data.error);
		        	}
		        	
		       },
	        errors: (jqXHR, textStatus, errorThrown) => {
	        	console.log('ERROR CALL BACK from outside')
	        	console.log(arguments);
	        }
		    }); //ajax


			
		}); //dispatch

	}//onFormSubmit
});

class BusinessOnSaleForm extends React.Component {
	constructor() {
		super(); 
		// 1) super(); is to be able to call [this] in constructor
		// 2) super(props); is to be able to call this.props in constructor
		// 3) this && this.props is binded everywhere else except in constructor();
	};

	render() {
		const { 
			onImageUpload,
			onRemoveImage,
      onFormSubmit,
      onSalonChange,
      onBack,
			salon,
			modal
		} = this.props;
console.log('FORM RENDERED', salon)
		let priceInput;
		let button;
			if (modal.open) {
				return(
					<div id="business-on-sale-form">
						<div className="form-wrapper">
							<form
								onSubmit={(ev) => {
									ev.preventDefault();
									console.log('Form Submitted');
									button.disabled = true;
									onFormSubmit(salon);
								}}
							> 
								
								<button 
									className="back mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
									onClick={() => onBack()}
								>
								  Back
								</button>
								<div 
									className="ui icon message"
									style={{ width: '50%'}}
								>
						      <i className="alarm outline icon"></i>
						      <div className="content">
						        <p>Please Post in English</p>
						      </div>
						    </div>
								<div className="form-header">
									<h1>Post My Salon</h1>
								</div>
								<div className="ui form" style={{ width: '90%'}}>
									<div className="field">
										<label>Title</label>
										<input 
											type="text" 
											required
											autoFocus={true}
											onChange={(ev) =>
												onSalonChange({ job: { title: ev.target.value }})
											}
										/>
									</div>
									<div className="field">
										<label>Description</label>
										<textarea
											required
											onChange={(ev) =>
												onSalonChange({ job: { description: ev.target.value}})
											}
										>
										</textarea>
									</div>
									<div className="field">
										<label>Contact Email</label>
										<input
											required
											type="email" 
											onChange={(ev) =>
												onSalonChange({ job: { contact_email: ev.target.value}})
											}
										/>
									</div>

									<div className="field">
										<label
											onClick={() => {


												if (salon.job.price && salon.job.price.length >= 8) { 
													priceInput.value = '';
													onSalonChange({ job: { price: 0 }})
													this.forceUpdate();
												}

											}}
										>
											{`Price ${(salon.job.price && salon.job.price.length >= 8) ? '(Click to Reset)' : ''}`}
										</label>
										
										<input 
											ref={ref => priceInput = ref}
											disabled={(salon.job.price && salon.job.price.length >= 8)}
											type="number" 
											min="1" 
											step="any"
											onChange={(ev) => {
												if (priceInput.value.length > 8) {
													return;
												} 
												onSalonChange({ job: { price: ev.target.value}})
												this.forceUpdate();
											}}
										/>
									</div>

									<div className="field">
										<label>Salon | Company Name</label>
										<input 
											required
											type="text"
											onChange={(ev) =>
												onSalonChange({ company_attributes: { name: ev.target.value}})
											} 
										/>
									</div>
									<div className="field">
										<label>Address Line 1</label>
										<input 
											type="text" 
											onChange={(ev) =>
												onSalonChange({ address_attributes: { address_line_1: ev.target.value }})
											}
										/>
									</div>
									<div className="field">
										<label>Address Line 2</label>
										<input 
											type="text" 
											onChange={(ev) =>
												onSalonChange({ address_attributes: { address_line_2: ev.target.value}})
											}
										/>
									</div>
									<div className="field">
										<label>City</label>
										<select 
											className="ui fluid dropdown"
											required
											onChange={ev => onSalonChange({city: ev.target.value})}
										>
											{
												[""].concat(salon.city_attributes).map(city => 
													<option 
														key={city} 
														className="option-city"
													>
														{ city }
													</option>
												)
											
											}
										</select>
									</div>
									<div className="field">
										<label>State</label>
										<h5 className="state-name" style={{ margin: 0}}>
											{
												`${salon.state.charAt(0).toUpperCase()}${salon.state.slice(1)}`
											}
										</h5>
									</div>
								</div>
								<hr />
								<div className="upload-pictures">
									<h2>Upload Pictures</h2>
									<DropzoneWrapper
										images={salon.images}
										onImageUpload={onImageUpload}
										onRemoveImage={onRemoveImage}
									/>
								</div>
								<hr />
								<button 
									className="post mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect"
									ref={ref => button = ref}
								>
								  Post My Salon
								</button>
								<div style={{height: '7em'}}>
								</div>
							</form>
						</div>
					</div>
				);
			} else {
				return null;
			};

	};
};


export default connect(
	mapStateToProps,
	mapDispatchToProps
)(BusinessOnSaleForm);


