import 'whatwg-fetch'; 
const { connect } = require('react-redux');
const AutosuggestWrapper = require('../components/AutosuggestWrapper.jsx').default;
const { 
	getSuggestionValue, 
	setItems,
	loaderOn,
	apiFetchSuggestions
} = require('../actions/usedItemsActionCreators');

const mapStateToProps = ({ suggestions, initialSearch }) => ({
	suggestions,
	initialSearch
});
const mapDispatchToProps = (dispatch) => ({
	fetchSuggestions(value) {
		dispatch(
			apiFetchSuggestions(`/api/used_items/suggestions.json?suggestion=${value}`)
		);
	},
	getSuggestionValue(suggestion) {
		dispatch(loaderOn());
		const url = `/api/used_items/search.json?search_value=${suggestion}`;
		getSuggestionValue(url)
		.then(items => {
			dispatch(setItems(items))
		})
		.catch(error => {
			console.log("Request for fetching Used Items failed: ", error)
		});
		return suggestion;
	},
});

const SearchBoxContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(AutosuggestWrapper);

export default SearchBoxContainer;

