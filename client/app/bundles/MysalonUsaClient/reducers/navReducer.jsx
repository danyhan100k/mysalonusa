import { combineReducers } from 'redux';

const mode = (state={
	open: false
}, action) => {
	switch(action.type) {
		case 'TOGGLE_MODE':
			return Object.assign({}, state, { open: !state.open })
		default:
			return state;
	};
}

const content = (state={}, action) => {
	switch(action.type) {
		default:
			return state;
	};
};

const language = (state={}, action) => {
	switch(action.type) {
		default:
			return state;
	};
};

const navReducer = combineReducers({
	mode,
	content,
	language
});

export default navReducer;
