import { combineReducers } from 'redux';
import { HELLO_WORLD_NAME_UPDATE } from '../constants/helloWorldConstants';

const name = (state = '', action) => {
  switch (action.type) {
    case HELLO_WORLD_NAME_UPDATE:
      return action.text;
    default:
      return state;
  }
};

const helloWorldReducer = combineReducers({ 
	name 
});

// function combineReducers(modalState){
// 	return function(state, action) { //helloWorldReducer
// 		return Object.keys(modalState).reduce((initial, key) => (
// 			initial[key] = modalState[key](state[key], action)
// 		), {})
// 	};
// };
export default helloWorldReducer;
