import { combineReducers } from 'redux';
import { suggestions, initialSearch, loader } from '../shared/shared.js';

const salons = (
	state={ 
		salons: [],
		length: 0,
		currentPage: 1,
		perPage: 20,
		totalEntries: 0,
		searchedValue: ''
	}, 
	action
) => {
	switch(action.type) {
		case 'SET_SALONS':
			return Object.assign({}, action.salons);
		default: 
			return state;
	};
};

const modal = (
	state={
		open: false,
		salon: {}
	},
	action
) => {
	switch(action.type) {
		case 'ON_SALON_CLICK':
			return { 
				open: true, 
				salon: action.salon
			};
		case 'ON_MODAL_EXIT':
			return {
				open: false,
				salon: {}
			}
		default:
			return state;
	};
};

export default combineReducers({
	suggestions,
	salons,
	modal,
	initialSearch,
	loader
});
