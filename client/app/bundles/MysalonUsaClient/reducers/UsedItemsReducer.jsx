import { combineReducers } from 'redux';
const { 
	suggestions, 
	initialSearch, 
	loader 
} = require('../shared/shared.js');

const usedItems = (
	state={ 
		usedItems: [],
		length: 0,
		currentPage: 1,
		perPage: 20,
		totalPages: 0,
		totalEntries: 0,
		searchedValue: ''
	}, 
	action
) => {
	switch(action.type) {
		case 'SET_ITEMS':
			return Object.assign({}, action.items);
		default: 
			return state;
	};
};

export const rootReducer = combineReducers({
	suggestions,
	initialSearch,	
	usedItems,
	loader,
});      
