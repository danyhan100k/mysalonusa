import { combineReducers } from 'redux';
import uuidV4 from 'uuid/v4';

const images = (state, action) => {
	switch(action.type) {
		case 'ON_IMAGE_UPLOAD':
			const newImages = action.images.map(img => (
				Object.assign({}, img, { id: uuidV4() })
			));
			return state.concat(newImages);
		case 'ON_REMOVE_IMAGE':
			return state.filter(img => {
				return (img.id !== action.image.id)
			});
		default: 
			return state;
	};
};

const salon = (
    state = initialSalon(uuidV4()),
    action
  ) => {
  switch(action.type) {
    case 'ON_IMAGE_UPLOAD':
      console.log(action.images);
      const newImages = action.images.map(file => (
              {
                id: uuidV4(),
                file
              }
            ));

      return Object.assign({}, state, {
        images: state.images.concat(newImages)
      });
    case 'ON_REMOVE_IMAGE':
      const images = state.images.filter(img =>
        img.id !== action.image.id
      );
      return Object.assign({}, state, { images });
    case 'ON_SALON_CHANGE':
        if (action.value.hasOwnProperty('job')) {
          state.job = Object.assign({}, state.job, action.value.job);
          return state;
        };

        if (action.value.hasOwnProperty('address_attributes')) {
          state.address_attributes = Object.assign({}, state.address_attributes, action.value.address_attributes);
          return state;
        };
        return Object.assign({}, state, action.value);
    case 'SALON_CREATED':
      return state;
    case 'ON_FETCHED_CITIES':
      return Object.assign({}, state, { state: action.state} , action.cities);
    default:
      return state;
  }
};

const modal = (
  state={ open: false }, 
  action
) => {
  switch(action.type) {
    case 'ON_FETCHED_CITIES':
      return { open: true };
    case 'ON_BACK':
    case 'SALON_CREATED':
      return { open: false };
    default:
      return state;
  }; 
};

const createdSalon = (
  state={}, 
  action
) => {
  switch(action.type) {
    case 'SALON_CREATED':
      return action.createdSalon;
    default:
      return state;
  };
};

const spinner = (
  state={ open: false },
  action
) => {
  switch(action.type) {
    case 'SHOW_SPINNER':
      return { open: true };
    case 'SALON_CREATED':
      return { open: false };
    default: 
      return state;
  };
};

const createBusinessOnSaleReducer = combineReducers({
	salon,
  modal,
  createdSalon,
  spinner
});

export default createBusinessOnSaleReducer;

function initialSalon(id) {
	return {
		id,
		images: [], 
    job: {},
    address_attributes: {},
    company_attributes: {},
	};
};

function createdSalonForTest() {
  return {
    company: {
      name: "great company",
      user: {
        email: "email@gmail.com"
      }
    },
    contact_email: "contact@gmail.com",
    created_at: "03/21/2017 04:38AM",
    created_at_utc: "2017-03-21 05:23:50 UTC",
    description: "this is description",
    images: [
      {
        url: "https://mysalonusa.s3.amazonaws.com/uploads/business_on_sale/images/158/colorchart.png"
      },
      {
        url: "https://s3.us-east-2.amazonaws.com/mysalonusa/uploads/business_on_sale/images/1/14691090_226293327789755_1396086586252341255_n.jpg"
      }
    ],
    price: "$100,000.00",
    title: "Amazing Salon on sale!"
  };
};
