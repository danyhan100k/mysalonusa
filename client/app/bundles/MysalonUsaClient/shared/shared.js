
export const thunk = ({ dispatch, getState }) => {
	return (next) => (action) => {
		if (typeof action === 'function') {
			return action(dispatch, getState);
		};
		return next(action);
	}
};

export const suggestions = (state=[], action) => {
	switch(action.type) { 
		case 'SET_SUGGESTIONS':
			return action.suggestions;
		default: 
			return state;
	};
};

export const loader = (state=false, action) => {
	switch(action.type) {
		case 'LOADER_ON':
			return true;
		case 'SET_SALONS':
		case 'SET_ITEMS':
			return false;
		default: return state;
	};
};

export const initialSearch = (state='', action) => (state);

export const formatDate = (date) => {
	const month = date.getUTCMonth() + 1; //months from 1-12
	const day = date.getUTCDate();
	const year = date.getUTCFullYear();
	const newdate = `${month}/${day}/${year}`;
	return newdate;
};