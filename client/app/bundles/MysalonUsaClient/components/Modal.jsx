import React from 'react';
import TimeAgo from 'react-timeago';

const Modal = ({ open, salon, onModalExit }) => {
	if (open) {
		let style;
		let mq = window.matchMedia( "(min-width: 992px)" );
		const created_at = new Date(salon.created_at_utc);

		if (mq.matches) {
			style = { width: '70%', 'overflow-y': 'scroll'};
		} else {
			style = { width: '92%', 'overflow-y': 'scroll'};
		}; 
		return(
			<div className="modalContainer">
				<div 
					className="popUpModal"
					style={style}
				>
					<div>
						<i 
							className="exit-icon remove icon"
							onClick={() => onModalExit()}
						>
						</i>
					
						<h2 className="ui center aligned icon header">
				      <i className="home icon"></i>
				      <div 
				      	className="content"
				      	style={{ width: '70%', margin: '0 auto', 'wordBreak': 'break-all'}}
				      >
				      	<p style={{fontSize: '.7em'}}>
				        { salon.title }
				        </p>
				        <div className="sub header">
				        </div>
				      </div>
				    </h2>
					</div>

					<div style={{marginTop: '1.7rem'}}>
			    	<div 
			    		style={{float: 'left', width: '50%'}}
			    		className="description">
			    		<div 
			    			className="description-inner-box"
			    			style={{ width: '87%', margin: '0 auto'}}
			    		>
				    		<p>
				    			{salon.description}
				    		</p>
				    	</div>
			    	</div>

			    	<div style={{float: 'left', width: '50%', marginTop: 0}}>
			    	<div 
			    		className="ui divided selection list"
			    		style={{width: '85%', margin: '0 auto'}}
			    	>
				      <a className="item">
				        <div className="ui teal horizontal label">Salon Name</div>
				        { salon.company_name }
				      </a>
				      <a className="item">
				        <div className="ui teal horizontal label">Address</div>
				        {`${salon.address_line_1} ${salon.address_line_2 == null ? '': salon.address_line_2}`}
				      </a>
				      <a className="item">
				        <div className="ui teal horizontal label">City</div>
				        { `${salon.city_name}`}
				      </a>
				      <a className="item">
				        <div className="ui teal horizontal label">State</div>
				        { `${salon.state_name}`}
				      </a>
				      <a className="item">
				        <div className="ui teal horizontal label">Zip</div>
				        { `${salon.zip}`}
				      </a>
				      <a className="item">
				        <div className="ui teal horizontal label">Price</div>
				        { `${salon.price == null ? 'private' : salon.price}`}
				      </a>
				       <a className="item">
				        <div className="ui teal horizontal label">Email</div>
				        { salon.email }
				      </a>
				      <a className="item">
				        <div className="ui horizontal label">Posted</div>
				        <TimeAgo date={created_at.toString()} />
				      </a>
				    </div>
				    <div 
				    	id="clear"
				    	style={{'clear': 'both'}}
				    ></div>
			   </div>
			   </div>



				</div>


			</div>
		);
	} else {
		return null
	};
};

export default Modal;









