import React from 'react';

const NotFound = ({msg}) => {
	return(
		<div className="business-on-sale message-container">
			<div className="message-wrapper ui icon message">
	      <i className="notched circle loading icon"></i>
	      <div className="content">
	        <div className="header">
	          0 Found
	        </div>
	        <div 
	        	className="suggestion">
		        <p>Try Searching:</p>
		        <h4 className="ui header">
		        	{ msg }
		        </h4>
		      </div>
	      </div>
	    </div>
    </div>
	);
};

export default NotFound;