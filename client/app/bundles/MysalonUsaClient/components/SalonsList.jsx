import React from 'react';
import SalonCard from './SalonCard';
import Pagination from 'react-js-pagination';
import DotLoader from './DotLoader';
import NotFound from '../components/NotFound';

class SalonsList extends React.Component {
	constructor() {
		super();
		//1) super(); is to be able to call [this] in constructor
		//2) super(props); is to be able to call props in constructor
		//3) this && this.props are set anywhere else.
		
		// function SalonsList() {
		// 	return React.component.apply(this, [props])
		// };
		// SalonsList = new SalonsList();
	};

	render() {
		const {
			salons,
			loader,
			pageChange,
			updateViewCount
		} = this.props;
		if (loader) {
			return <DotLoader open={loader} />
		};
		if (salons.salons.length > 0) {
			return(
				<div className={"mdl-grid business-on-sale-container"}>
					{
						salons.salons.map(salon =>
							<SalonCard
								key={salon.business_on_sale_id}
								salon={salon}
								updateViewCount={updateViewCount}
							/>
						)
					}
					<div className="mdl-cell--12-col">
						<Pagination
							className="pagination"
							activePage={salons.currentPage}
							itemsCountPerPage={salons.perPage}
							totalItemsCount={salons.totalEntries}
							onChange={(page) => {
							 pageChange(page, salons.searchedValue);
							}}
						/>
					</div>
				</div>
			);
	} else {
			return <NotFound msg={"California, Illinois, New York"}/>
		}
	};
};

export default SalonsList;