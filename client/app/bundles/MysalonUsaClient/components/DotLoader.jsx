import React from 'react';
import Loader from 'halogen/DotLoader';

export default ({ open }) => {
	
	return(
		open ? 
			<div className="halogen-loader-container">
				<div className="halogen-loader-wrapper">
					<Loader 
						className="halogen-loader" 
						color="#ff4081" 
						size="80px" 
						/>
				</div>
			</div>
		:
			null
	);
};