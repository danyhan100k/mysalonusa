const React = require('react');
const Dropzone = require('react-dropzone');

const DropzoneWrapper = React.createClass({
  onDrop: function (acceptedFiles) {
    this.props.onImageUpload(acceptedFiles);
  },

  onOpenClick: function () {
    this.dropzone.open();
  },

  render: function () {
    const { 
      images,
      onRemoveImage
    } = this.props;
    console.log('dropzone rendered', images);

    return (
      <div>
        <Dropzone ref={(node) => { this.dropzone = node; }} onDrop={this.onDrop}>
          <div>Drop Pictures here or click to upload pictures!</div>
        </Dropzone>
        <button type="button" onClick={this.onOpenClick}>
           Upload Images
        </button>
        {
          images.length > 0 ?
            <div>
              <h2>
                Uploading {images.length} images...
                (Click to Remove Image)
              </h2>
              <div>
                {
                  images.map(image => 
                    <img 
                      key={image.id}
                      src={image.file.preview} 
                      width="100"
                      height="100"
                      onClick={() => onRemoveImage(image)}
                    />
                  )
                }
              </div>
            </div>
          :
            null
        }
      </div>
    );
  }
});

export default DropzoneWrapper;
