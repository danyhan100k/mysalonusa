const React = require('react');
const TimeAgo = require('react-timeago').default;
const { formatDate } = require('../shared/shared.js'); 

function showModal(salon) {
	let title = salon.title;
	let description = salon.description;
	let email = salon.email;
	let address = salon.address_line_1 + (salon.address_line_2 || "");
	let cityStateZip = `${salon.city_name} ${salon.state_name} ${salon.city_zip}`;
	let companyName = salon.company_name;
	let createdAt = new Date(salon.created_at_utc.replace(/-/g, "/")).toLocaleString();
	if ($(window).width() >= 992) {
		createdAt = jQuery.timeago(createdAt);
	};
	let price = salon.price || 'Private';
	let images = ["<div class='modal-images-wrapper'>"];
	salon.images.forEach(img => {
		images.push(`<a href=${img} target='_blank'><img class='ui medium image' src=${img} style='width: 100%; margin: auto;'></a>`);
	});
	images.push(`</div>`);
	const modal = 
			"<div class='modal fade' tabindex='-1' role='dialog'>" +
			  "<div class='modal-dialog' role='document'>" +
			    "<div class='modal-content'>" +
			      "<div class='modal-header' style='word-break: break-all'>" +
			        "<button type='button' class='close' data-dismiss='modal' aria-label='Close'>" +
			        	"<span aria-hidden='true'>&times;</span>" +
			        "</button>" +
			        "<h3 class='modal-title'>" + salon.title + "</h3>" +
			      "</div>" +
			      "<div class='modal-body'>" +
			        "<p class='job-modal-description'>" + salon.description + 
			        "</p>" +
			      "</div>" +
			       "<div class='list-group'>" + 
			        "<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Contact Email: </div>"+ "<a href='mailto:" + email + "'>" + email + "</a>" + "</button>" + 
			        "<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Salon Name: </div>"+ companyName + "</button>" +
			        "<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Address: </div>"+ address + "</button>" + 
 							"<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>City & State: </div>"+ cityStateZip + "</button>" +
 							"<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Price: </div>"+ price + "</button>" + 
 							"<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Posted Date: </div>"+ createdAt + "</button>" +
 							images.join('') +
			      "</div>" + 
			      "<div class='modal-footer' style='border-top: none;'>" +
			        "<button type='button' class='btn btn-primary' data-dismiss='modal' style='background-color: #ee5c8e; border-color: #ee5c8e;'>Close</button>" +
			      "</div>" +
			    "</div>" +
			  "</div>" +
			"</div>";
			return modal;
};

const SalonCard = React.createClass({
	render() {
		const { 
			salon,
			updateViewCount
		} = this.props;
		const created_at = new Date(salon.created_at_utc);
		let imgSrc;
		let createdAtForMobile;
		if (salon.images && salon.images.length > 0) {
			imgSrc = salon.images[0]
		} else {
			imgSrc = "https://s3.us-east-2.amazonaws.com/mysalonusa/uploads/cities/image.png";
		};
		if ($(window).width() <= 992) {
			createdAtForMobile = formatDate(
				new Date(
					salon.created_at_utc.replace(/-/g, "/")
				)
			);
		};
		return( 
			<div 
				className="col-xs-12 col-sm-6 col-md-3"
				style={{ marginBottom: '2em'}}
			>
				<div 
					className="item"
					style={{ border: '1px solid #e6e6e6'}}
				>
	        <div className="image">
	          <img 
	          	style={{ width: '100%', height: '200px'}}
	          	src={imgSrc} />
	        </div>
	        <div className="content">
	          <a className="header">
	          	<h4 style={{ margin: '.7em'}}>
	          		{ `${truncate(salon.company_name, 0,28)}` }
	          	</h4>
	          </a>
	          <div className="meta">
	            <h5>
	            	{ 
		 							`${salon.city_name}, 
		 					 		 ${salon.state_abbreviation.charAt(0).toUpperCase() + salon.state_abbreviation.slice(1)}`
		 					 	}
	            </h5>
	          </div>
	          
	          <div className="extra">
	          	<div 
	          		className="ui segments"
	          		style={{
	          			marginTop: '1em', 
	          			borderLeft: '0px solid',
	          			borderRight: '0px solid', 
	          			borderBottom: '0px solid'
	          		}}
	          	>
	          		<div 
	          			className="ui segment"
	          			style={{ wordBreak: 'break-all', margin: '0.2em', padding: '1em'}}
	          		>
					        { truncate(salon.title, 0, 35)}
					      </div>
					      <div 
					      	className="ui segment"
					      	style={{ padding: '1em'}}
					      >
					        <p>{salon.price ? salon.price : 'Price Private'}</p>
					      </div>
					      <div 
					      	className="ui segment"
					      	style={{ padding: '1em'}}
					      >
					        <p style={{ color: '#00b5ad' }}>{salon.views} views</p>
					      </div>
					      <div 
					      	className="ui segment"
					      	style={{ padding: '1em' }}
					     	>
					        <p>
					        	{
					        		createdAtForMobile ? `Posted: ${createdAtForMobile}`
					        		:
					        		<TimeAgo 
												date={created_at.toString()} 
											/>
					        	}
									</p>
					      </div>
					    </div>


							<div className="mdl-card__actions mdl-card--border">
								<a 
									className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
									onClick={() => {
										$(showModal(salon)).modal();
										updateViewCount(salon.id);
									}}
								>
								  View
								</a>
				 			</div>

	          </div>
	        </div>
	      </div>
	    </div>
		);
	}
});




function truncate(text, start,end) {
	if (text.length > end) {
		return `${text.slice(start, end)}..`
	} else {
		return text;
	}
}
export default SalonCard;