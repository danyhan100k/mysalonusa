import React, { PropTypes } from 'react';
import Autosuggest from 'react-autosuggest';

const renderSuggestion = suggestion => (
  <div>
    { suggestion }
  </div>
);

class AutosuggestWrapper extends React.Component {
	constructor(props) {
		super(props);
		// let component = new AutosuggestWrapper();
		// function Autocomplete() {
		// 	super() == React.Component.apply(this, [props])
		// };
		// super() allows use of this in constructor
		// super(props) allows use of this.props in constructor
		this.props.getSuggestionValue(this.props.initialSearch);
		this.state = {
			value: ''
		};
	};

	onChange = (event, { newValue }) =>  {
		this.setState({
			value: newValue
		});
	};

	onSuggestionsFetchRequested = ({ value }) => {
		this.props.fetchSuggestions(value);
	};

	onSuggestionsClearRequested() {
		// console.log('onSuggestionsClearRequested is called when you need to clear suggestions')
	};

	render() {
		const { value } = this.state;
		const { 
			suggestions, 
			getSuggestionValue 
		} = this.props;
		const inputProps = {
			placeholder: 'ex) New York, California, Chicago',
    	value,
    	onChange: this.onChange
		}; 

		return(
			<div className="autosuggest-container">
				<form
					onSubmit={ev => {
						ev.preventDefault();
						getSuggestionValue(this.state.value);
					}}
				>
					<h1 className="ui header">
						Search
					</h1>
					<div className="ui big icon input">
						<Autosuggest
			        suggestions={suggestions}
			        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
			        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
			        getSuggestionValue={getSuggestionValue}
			        renderSuggestion={renderSuggestion}
			        inputProps={inputProps}
			      />
			      <button 
			      	className="mdl-button 
			      	mdl-js-button 
			      	mdl-button--raised 
			      	mdl-button--colored"
			      	style={{ height: '3.2em', width: '10em'}}
			      >
						  Search
						</button>
		      </div>
		      <div style={{ margin: '1em 0'}}>
		      Please Search in English
		      </div>
		     </form>
	    </div>
		); 
	};
}
export default AutosuggestWrapper;