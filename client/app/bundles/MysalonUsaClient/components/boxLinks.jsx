const React = require('react');

const BoxLinks = ({ firstLink, secondLink }) => {
	return(
		<div className="used-items-footer-links ui grid container">
			<div className="ui three bottom attached steps">
			  <div className="step">
			    <i className="upload icon"></i>
			    <div className="content">
			      <div className="title">
			      	<a href="">
			      		{ firstLink }
			      	</a>
			      </div>
			    </div>
			  </div>
			  <div className="active step">
			    <i className="world icon icon"></i>
			    <div className="content">
			      <div className="title">
			      		{ secondLink }
			      </div>
			    </div>
			  </div>
			</div>
		</div>
	);
};

export default BoxLinks;