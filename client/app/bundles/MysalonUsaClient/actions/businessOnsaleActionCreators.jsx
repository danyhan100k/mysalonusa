import 'whatwg-fetch'; 
// export const updateName = (text) => ({ 
//   type: HELLO_WORLD_NAME_UPDATE,
//   text,
// });

export const fetchSalons = (searchValue, page) => (dispatch, getState) => {
	fetch(`api/business_on_sales/salons.json?search_value=${searchValue}&page=${page}`)
	.then(response => {
		if (response.status >= 200 && response.status < 300) {
			return response.json();
		} else {
			let error = new Error(response.statusText);
			throw error
		}
	})
	.then(salons => { 
		console.log('%c SALONS RECEIVED: ', 'color: green', salons);
		dispatch({
			type: 'SET_SALONS',
			salons
		});
	})
	.catch(error => {
		console.log('Request for fetching Salons failed: ', error);
	})

	return suggestion;
};


export const fetchSalons2 = (searchValue, dispatch, page) => {
	fetch(`api/business_on_sales/salons.json?search_value=${searchValue}&page=${page}`)
	.then(response => {
		if (response.status >= 200 && response.status < 300) {
			return response.json();
		} else {
			let error = new Error(response.statusText);
			throw error
		}
	})
	.then(salons => { 
		console.log('%c SALONS RECEIVED: ', 'color: green', salons);
		dispatch({
			type: 'SET_SALONS',
			salons
		});
	})
	.catch(error => {
		console.log('Request for fetching Salons failed: ', error);
	})

	return suggestion;
};


// export function fetchSuggestions(dispatch) {
// 	return (value) => {
// 		fetch(`api/business_on_sales/suggestions.json?suggestion=${value}`)
// 		.then((response) => {
// 			if (response.status >= 200 && response.status < 300) {
// 				return response.json();
// 			} else {
// 				let error = new Error(response.statusText);
// 				throw error
// 			}
// 		})
// 		.then((suggestions) => {
// 			dispatch({
// 				type: 'SET_SUGGESTIONS',
// 				suggestions
// 			});
// 		})
// 		.catch((error) => {
// 			console.log('Request for autosuggest failed: ', error)
// 		});
// 	};
// };

export const fetchSuggestions = (value) => (dispatch, getState) => {
	fetch(`api/business_on_sales/suggestions.json?suggestion=${value}`)
	.then(response => {
		if (response.status >= 200 && response.status < 300) {
			return response.json();
		} else {
			let error = new Error(response.statusText);
			throw error
		}
	})
	.then(salons => { 
		console.log('%c SALONS RECEIVED: ', 'color: green', salons);
		dispatch({
			type: 'SET_SALONS',
			salons
		});
	})
	.catch(error => {
		console.log('Request for fetching Salons failed: ', error);
	})

	return va;
};

