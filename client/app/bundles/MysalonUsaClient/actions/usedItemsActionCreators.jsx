import 'whatwg-fetch'; 

export const getSuggestionValue = (url) => {
	return fetch(url).then(response => {
		if (response.status >= 200 && response.status < 300) {
			return response.json();
		} else {
			let error = new Error(response.statusText);
			throw error
		};
	})
};

export const setItems = (items) => ({
	type: 'SET_ITEMS',
	items
});

export const setSalons = (salons) => ({
	type: 'SET_SALONS',
	salons
});

export const loaderOn = () => ({ type: "LOADER_ON" });

export const setSuggestions = (suggestions) => ({ 
	type: "SET_SUGGESTIONS", 
	suggestions
});

export const apiFetchSuggestions = (url) => (dispatch, getState) => {
	fetch(url)
		.then((response) => {
			if (response.status >= 200 && response.status < 300) {
				return response.json();
			} else {
				let error = new Error(response.statusText);
				throw error
			}
		})
		.then((suggestions) => {
			dispatch(setSuggestions(suggestions));
		})
		.catch((error) => {
			console.log('Request for autosuggest failed: ', error)
		});
};