/* eslint-disable import/prefer-default-export */

import { HELLO_WORLD_NAME_UPDATE } from '../constants/helloWorldConstants';

// const props = { updateName: dispatch({ type: '', text })}
export const updateName = (text) => ({ 
  type: HELLO_WORLD_NAME_UPDATE,
  text,
});

export const something = () => ({
	type: "coolio"
})
