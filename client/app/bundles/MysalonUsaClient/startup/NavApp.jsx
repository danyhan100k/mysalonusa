import React from 'react';
import { Provider } from 'react-redux';
import configureStore from '../store/NavStore';
import NavMenuContainer from '../containers/NavMenuContainer.jsx';

const NavApp = (props, _railsContext) => {
	return(
		<Provider store={configureStore(props)}>
			<NavMenuContainer />
		</Provider>
	);
}; 

export default NavApp;
