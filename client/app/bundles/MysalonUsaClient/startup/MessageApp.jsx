import React from 'react';
import { Provider } from 'react-redux';
import configureStore from '../store/messageStore';
//configureStore creates store, takes action

// props = initial state for store reducer
const MessageApp = (props, _railsContext) => {
	return( //props is initial state
		<Provider store={configureStore(props)}> 
			<MessageContainer />
		</Provider>
	);
};

export default MessageApp;