import ReactOnRails from 'react-on-rails';

import HelloWorldApp from './HelloWorldApp';
import MessageApp from './MessageApp';
import SearchBusinessOnSaleApp from './SearchBusinessOnSaleApp';
import CreateBusinessOnSaleApp from './CreateBusinessOnSaleApp';
import NavMenuContainer from '../containers/NavMenuContainer.jsx';
import NavApp from './NavApp';
import SearchUsedItemApp from './SearchUsedItemApp';

ReactOnRails.register({
	NavApp,
  SearchBusinessOnSaleApp,
  CreateBusinessOnSaleApp,
  SearchUsedItemApp,
});
