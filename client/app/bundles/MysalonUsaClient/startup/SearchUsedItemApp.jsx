import React from 'react';
import { Provider } from 'react-redux';
import configureStore from '../store/UsedItemStore';
import SearchBoxContainer from '../containers/SearchBoxContainer';
import UsedItemsListContainer from '../containers/UsedItemsListContainer';
const SearchUsedItemApp = (props, _railsContext) => {
	return(
		<Provider store={configureStore(props)}>
			<div>
				<SearchBoxContainer />
				<UsedItemsListContainer />
			</div>
		</Provider>
	);
};

export default SearchUsedItemApp;