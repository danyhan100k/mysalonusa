import React from 'react';
import { Provider } from 'react-redux';
import configureStore from '../store/CreateBusinessOnSaleStore';
import BusinessOnSaleFormRenderer from '../containers/BusinessOnSaleFormRenderer';

import BusinessOnSaleFormContainer from '../containers/BusinessOnSaleFormContainer';
import StateOptionsContainer from '../containers/StateOptionsContainer';

const CreateBusinessOnSaleApp = (props, _railsContext) => {
	return( 
		<Provider store={configureStore(props)}>
			<BusinessOnSaleFormRenderer />
		</Provider>
	);
};

export default CreateBusinessOnSaleApp;