import React from 'react';
import { Provider } from 'react-redux';
import configureStore from '../store/BusinessOnSaleStore';
import SearchBoxContainer from '../containers/BusinessOnSaleSearchBoxContainer';
import SalonsList from '../containers/SalonsListContainer';
import ModalContainer from '../containers/ModalContainer';

const SearchBusinessOnSaleApp = (props, _railsContext) => {
	return(
		<Provider store={configureStore(props)}>
			<div>
				<SearchBoxContainer />
				<SalonsList />
				<ModalContainer />
			</div>
		</Provider>
	);
};

export default SearchBusinessOnSaleApp;