# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170819062838) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "addresses", force: :cascade do |t|
    t.text     "address_line_1"
    t.text     "address_line_2"
    t.integer  "city_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "addresses", ["city_id"], name: "index_addresses_on_city_id", using: :btree

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "business_on_sale_payments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "business_on_sale_plan_id"
    t.string   "description"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "payment_customer_id"
    t.string   "payment_charge_id"
  end

  add_index "business_on_sale_payments", ["business_on_sale_plan_id"], name: "index_business_on_sale_payments_on_business_on_sale_plan_id", using: :btree
  add_index "business_on_sale_payments", ["user_id"], name: "index_business_on_sale_payments_on_user_id", using: :btree

  create_table "business_on_sale_plans", force: :cascade do |t|
    t.integer  "name"
    t.integer  "total_tokens"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "USD", null: false
  end

  create_table "business_on_sales", force: :cascade do |t|
    t.integer  "company_location_sale_id"
    t.text     "title"
    t.text     "description"
    t.integer  "views",                    default: 0
    t.text     "contact_email"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "price_cents",              default: 0,     null: false
    t.string   "price_currency",           default: "USD", null: false
    t.json     "images"
  end

  add_index "business_on_sales", ["company_location_sale_id"], name: "index_business_on_sales_on_company_location_sale_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.integer  "type_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cities", force: :cascade do |t|
    t.integer  "state_id"
    t.string   "name"
    t.string   "zip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "companies", ["user_id"], name: "index_companies_on_user_id", using: :btree

  create_table "company_location_sales", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "address_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "company_location_sales", ["address_id"], name: "index_company_location_sales_on_address_id", using: :btree
  add_index "company_location_sales", ["company_id", "address_id"], name: "index_company_location_sales_on_company_id_and_address_id", using: :btree
  add_index "company_location_sales", ["company_id"], name: "index_company_location_sales_on_company_id", using: :btree

  create_table "company_locations", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "address_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "company_locations", ["address_id"], name: "index_company_locations_on_address_id", using: :btree
  add_index "company_locations", ["company_id"], name: "index_company_locations_on_company_id", using: :btree

  create_table "exception_trackers", force: :cascade do |t|
    t.integer "user_id"
    t.text    "description"
    t.text    "backtrace"
    t.text    "location"
  end

  add_index "exception_trackers", ["user_id"], name: "index_exception_trackers_on_user_id", using: :btree

  create_table "features", force: :cascade do |t|
    t.integer  "featureable_id"
    t.string   "featureable_type"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "features", ["featureable_type", "featureable_id"], name: "index_features_on_featureable_type_and_featureable_id", using: :btree
  add_index "features", ["featureable_type"], name: "index_features_on_featureable_type", using: :btree

  create_table "jobs", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "job_type"
    t.text     "contact_email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "views",               default: 0
    t.date     "created_at_reformat"
    t.integer  "company_location_id"
    t.string   "job_title"
  end

  add_index "jobs", ["company_location_id"], name: "index_jobs_on_company_location_id", using: :btree

  create_table "jobs_languages", id: false, force: :cascade do |t|
    t.integer "job_id"
    t.integer "language_id"
  end

  add_index "jobs_languages", ["job_id", "language_id"], name: "index_jobs_languages_on_job_id_and_language_id", unique: true, using: :btree

  create_table "languages", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "plan_id"
    t.string   "payment_customer_id"
    t.string   "payment_charge_id"
    t.string   "description"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "payments", ["plan_id"], name: "index_payments_on_plan_id", using: :btree
  add_index "payments", ["user_id", "plan_id"], name: "index_payments_on_user_id_and_plan_id", using: :btree
  add_index "payments", ["user_id"], name: "index_payments_on_user_id", using: :btree

  create_table "phone_numbers", force: :cascade do |t|
    t.integer  "phoneable_id"
    t.string   "phoneable_type"
    t.string   "number"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "phone_numbers", ["phoneable_type", "phoneable_id"], name: "index_phone_numbers_on_phoneable_type_and_phoneable_id", using: :btree

  create_table "plans", force: :cascade do |t|
    t.integer  "name"
    t.integer  "total_tokens"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "USD", null: false
  end

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "abbreviation"
  end

  create_table "subscriptions", force: :cascade do |t|
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "used_items", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "price_cents",    default: 0,     null: false
    t.string   "price_currency", default: "USD", null: false
    t.text     "contact_email"
    t.integer  "views"
    t.integer  "city_id"
    t.json     "images"
    t.integer  "category_id"
  end

  add_index "used_items", ["category_id"], name: "index_used_items_on_category_id", using: :btree
  add_index "used_items", ["city_id"], name: "index_used_items_on_city_id", using: :btree
  add_index "used_items", ["user_id"], name: "index_used_items_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "provider",                              null: false
    t.string   "uid",                                   null: false
    t.string   "name"
    t.string   "email"
    t.string   "location"
    t.string   "image_url"
    t.string   "prof_url"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.boolean  "private_tokens_access", default: false
    t.integer  "private_tokens_left",   default: 0
    t.integer  "status",                default: 1
    t.boolean  "admin",                 default: false
  end

  add_index "users", ["provider", "uid"], name: "index_users_on_provider_and_uid", unique: true, using: :btree
  add_index "users", ["provider"], name: "index_users_on_provider", using: :btree
  add_index "users", ["uid"], name: "index_users_on_uid", using: :btree

  add_foreign_key "addresses", "cities"
  add_foreign_key "business_on_sale_payments", "business_on_sale_plans"
  add_foreign_key "business_on_sale_payments", "users"
  add_foreign_key "business_on_sales", "company_location_sales"
  add_foreign_key "cities", "states"
  add_foreign_key "companies", "users"
  add_foreign_key "company_location_sales", "addresses"
  add_foreign_key "company_location_sales", "companies"
  add_foreign_key "company_locations", "addresses"
  add_foreign_key "company_locations", "companies"
  add_foreign_key "exception_trackers", "users"
  add_foreign_key "jobs", "company_locations"
  add_foreign_key "payments", "plans"
  add_foreign_key "payments", "users"
  add_foreign_key "used_items", "users"
end
