state_cities = [
  {
    "state": { "name": "illinois", "abbreviation": "il" },
    "cities": ["chicago", "libertyville"],

  },
  {
    "state": { "name": "california", "abbreviation": "cali" },
    "cities": ["los angeles", "long beach"]
  },
  {
    "state": { "name": "new york", "abbreviation": "ny" },
    "cities": ["new jersey"]
  }
]

plans = [{ 'total_tokens': 1, 'price': 20.00 },
         { 'total_tokens': 10, 'price': 40.00 },
         { 'total_tokens': 20, 'price': 70.00 }]


ActiveRecord::Base.transaction do
  Plan.destroy_all
  Company.destroy_all
  State.destroy_all

  plans.each_with_index do |plan, i|
    Plan.create!(
      name: i,
      total_tokens: plan[:total_tokens],
      price: plan[:price]
    )
  end

  state_cities.each do |state_hash|
    state = State.create!(
      name: state_hash[:state][:name],
      abbreviation: state_hash[:state][:abbreviation]
    )

    state_hash[:cities].each_with_index do |city, i|
      state.cities.create!(
        name: city,
        zip: SecureRandom.random_number(12345).to_s
      )
    end
  end

  City.find_each do |city|
    1.upto(40) do |i|
      company = Company.create! name: "Success Nail Salon.v#{i}"

      company_location = CompanyLocation.create!(
        company: company,
        address: Address.create!(
          address_line_1: "#{i.to_s * 3} street ave",
          city: city
        )
      )

      job_type = Job.job_types.keys.sample
      job = company_location.jobs.create!(
        title: ["Hiring ", "Wanted "].sample + job_type.gsub('_', ' ') + " nail technician",
        description: "lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eu vulputate lectus,
                      a luctus ex. Pellentesque lobortis dolor venenatis erat eleifend,
                      vitae condimentum tortor rhoncus.",
        contact_email: "user100k@gmail.com",                   
        job_type: job_type,
        views: i * 7
      )
      job.update! created_at: i.days.ago
    end
  end
end
