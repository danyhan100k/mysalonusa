class AddCompanyLocationToJobs < ActiveRecord::Migration
  def change
    add_reference :jobs, :company_location, index: true, foreign_key: true
  end
end
