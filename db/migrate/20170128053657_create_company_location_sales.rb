class CreateCompanyLocationSales < ActiveRecord::Migration
  def change
    create_table :company_location_sales do |t|
      t.references :company, index: true, foreign_key: true
      t.references :address, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :company_location_sales, [:company_id, :address_id]
  end
end
