class AddColToBusinessOnSalePayments < ActiveRecord::Migration
  def change
  	add_column :business_on_sale_payments, :payment_customer_id, :string
  	add_column :business_on_sale_payments, :payment_charge_id, :string
  end
end
