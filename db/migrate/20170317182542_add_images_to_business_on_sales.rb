class AddImagesToBusinessOnSales < ActiveRecord::Migration
  def change
    add_column :business_on_sales, :images, :json
  end
end
