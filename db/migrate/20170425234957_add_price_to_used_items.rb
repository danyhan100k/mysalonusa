class AddPriceToUsedItems < ActiveRecord::Migration
  def change
  	add_monetize :used_items, :price
  end
end
