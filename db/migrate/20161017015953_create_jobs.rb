class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.text :description
      t.integer :job_type
      t.text :contact_email
      t.belongs_to :company_state, index: true, foreign_key: true
      t.belongs_to :address, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
