class CreateLanguages < ActiveRecord::Migration
  def up
  	create_table :languages do |t|
      t.string :name

      t.timestamps null: false
    end

  	languages = ['English', 'Korean', 'Mongolian', 'Vietnamese']
  	languages.each do |language|
  		Language.create name: language
  	end
  end
  
  def down
  	Language.destroy_all
    drop_table :languages
  end
end
