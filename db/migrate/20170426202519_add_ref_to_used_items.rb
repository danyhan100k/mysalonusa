class AddRefToUsedItems < ActiveRecord::Migration
  def change
  	add_reference :used_items, :category, index: true
  end
end
