class CreateExceptionTrackers < ActiveRecord::Migration
  def change
    create_table :exception_trackers do |t|
      t.references :user, index: true, foreign_key: true
      t.text :description
      t.text :backtrace
      t.text :location
    end
  end
end
