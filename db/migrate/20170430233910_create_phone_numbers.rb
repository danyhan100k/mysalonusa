class CreatePhoneNumbers < ActiveRecord::Migration
  def change
    create_table :phone_numbers do |t|
      t.references :phoneable, polymorphic: true, index: true
      t.integer :number
      t.timestamps null: false
    end
  end
end
