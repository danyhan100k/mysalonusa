class CreateJobsLanguagesJoinTable < ActiveRecord::Migration
  def change
    create_table :jobs_languages, id: false do |t|
      t.integer :job_id
      t.integer :language_id
    end
    add_index :jobs_languages, [:job_id, :language_id], unique: true
  end
end
