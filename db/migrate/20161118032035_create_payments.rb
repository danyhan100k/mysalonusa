class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.references :plan, index: true, foreign_key: true
      t.string :payment_customer_id
      t.string :payment_charge_id
      t.string :description
      t.timestamps null: false
    end
  add_index :payments, [:user_id, :plan_id]
  end
end
