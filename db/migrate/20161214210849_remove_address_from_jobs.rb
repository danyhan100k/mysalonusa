class RemoveAddressFromJobs < ActiveRecord::Migration
  def change
    remove_reference :jobs, :address, index: true, foreign_key: true
  end
end
