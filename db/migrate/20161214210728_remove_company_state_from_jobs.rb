class RemoveCompanyStateFromJobs < ActiveRecord::Migration
  def change
    remove_reference :jobs, :company_state, index: true, foreign_key: true
  end
end
