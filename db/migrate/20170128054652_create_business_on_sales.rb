class CreateBusinessOnSales < ActiveRecord::Migration
  def change
    create_table :business_on_sales do |t|
      t.references :company_location_sale, index: true, foreign_key: true
      t.text :title
      t.text :description
      t.integer :views
      t.text :contact_email

      t.timestamps null: false
    end
  end
end
