class CreateFeatures < ActiveRecord::Migration
  def change
    create_table :features do |t|
      t.references :featureable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
