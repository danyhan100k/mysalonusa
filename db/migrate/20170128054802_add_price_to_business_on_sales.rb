class AddPriceToBusinessOnSales < ActiveRecord::Migration
  def change
  	add_monetize :business_on_sales, :price
  end
end
