class CreateCompanyLocations < ActiveRecord::Migration
  def change
    create_table :company_locations do |t|
      t.belongs_to :company, index: true, foreign_key: true
      t.belongs_to :address, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
