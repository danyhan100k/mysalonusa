class AddIndexToFeatures < ActiveRecord::Migration
  def change
  	add_index :features, :featureable_type
  end
end
