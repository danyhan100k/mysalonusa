class CreateBusinessOnSalePlans < ActiveRecord::Migration
  def change
    create_table :business_on_sale_plans do |t|
      t.integer :name
      t.integer :total_tokens

      t.timestamps null: false
    end
  end
end
