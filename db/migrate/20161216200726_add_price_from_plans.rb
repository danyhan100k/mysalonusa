class AddPriceFromPlans < ActiveRecord::Migration
  def change
    add_monetize :plans, :price
  end
end
