class AddImagesToUsedItem < ActiveRecord::Migration
  def change
    add_column :used_items, :images, :json
  end
end
