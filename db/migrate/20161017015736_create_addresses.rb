class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.belongs_to :company_state, index: true, foreign_key: true
      t.text :address_line_1
      t.text :address_line_2
      t.belongs_to :city, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
