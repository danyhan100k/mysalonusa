class AddColPrivateTokensToUsers < ActiveRecord::Migration
  def up
  	add_column :users, :private_tokens_access, :boolean, default: false
  	add_column :users, :private_tokens_left, :integer, default: 0
  	add_column :users, :status, :integer, default: 1
  end

  def down
  	add_column :users, :private_tokens_access, :boolean
  	remove_column :users, :private_tokens_left, :integer
  	remove_column :users, :status, :integer
  end
end
