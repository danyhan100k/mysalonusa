class ChangeColForBusinessOnSales < ActiveRecord::Migration
  def up
  	change_column :business_on_sales, :views, :integer, default: 0
  	BusinessOnSale.all.each do |x| 
  		if x.views.nil?
  			x.views = 0
  			x.save
  		end
  	end
  end

  def down
  	change_column :business_on_sales, :views, :integer
  end

end
