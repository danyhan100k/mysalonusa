class RemoveCompanyStateFromAddresses < ActiveRecord::Migration
  def change
    remove_reference :addresses, :company_state, index: true, foreign_key: true
  end
end
