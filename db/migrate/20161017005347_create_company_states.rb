class CreateCompanyStates < ActiveRecord::Migration
  def change
    create_table :company_states do |t|
      t.references :company, index: true, foreign_key: true
      t.belongs_to :state, index: true, foreign_key: true

      t.timestamps null: false
      t.index [:company_id, :state_id], unique: true
    end
  end
end
