class AddViewToUsedItems < ActiveRecord::Migration
  def change
  	add_column :used_items, :contact_email, :text 
    add_column :used_items, :views, :integer
  end
end
