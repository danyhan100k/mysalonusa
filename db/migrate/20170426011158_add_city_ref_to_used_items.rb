class AddCityRefToUsedItems < ActiveRecord::Migration
  def change
  	add_reference :used_items, :city, index: true
  end
end
