class AddPriceFromBusinessOnSalePlans < ActiveRecord::Migration
  def change
  	add_monetize :business_on_sale_plans, :price
  end
end
