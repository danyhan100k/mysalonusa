class CreateBusinessOnSalePayments < ActiveRecord::Migration
  def change
    create_table :business_on_sale_payments do |t|
      t.references :user, index: true, foreign_key: true
      t.references :business_on_sale_plan, index: true, foreign_key: true
      t.string :description

      t.timestamps null: false
    end
  end
end
