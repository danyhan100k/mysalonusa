class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.integer :name, unique: true
      t.integer :total_tokens
      t.decimal :price, :precision => 8, :scale => 2
      t.timestamps null: false
    end
  # add_column :plans, :price, :decimal, :precision => 8, :scale => 2
  end
end
