if Rails.env == 'production'
	url = ENV['BONSAI_OLIVE_URL']
  require 'elasticsearch/model'
  Elasticsearch::Model.client = Elasticsearch::Client.new url: url
  Searchkick.client = Elasticsearch::Client.new(hosts: url, retry_on_failure: true, transport_options: {request: {timeout: 250}})
end
