
Rails.application.config.middleware.use OmniAuth::Builder do
  provider(
    :auth0,
    'twHOFW5J6PJ2tATRJ7icXC106RXUVMX5',
    'gR2RYlElxO68Iu1DpwckJxbePwYiW0rh-cqmGTIG4ILXlhh7EdMM9TUF_w8MWeWG',
    'successnailsalon.auth0.com',
    callback_path: "/auth/auth0/callback"
  )
end