Rails.application.routes.draw do
  get 'hello_world', to: 'hello_world#index'
  ActiveAdmin.routes(self)
  root 'welcome#index'
  get '/coming_soon', to: 'welcome#coming_soon', as: 'coming_soon'

  get 'user/sign_in', to: 'sessions#sign_in', as: "sign_in"
  get "/auth/auth0/callback", to: 'sessions#callback' #lock callback
  get "/auth/failure", to: 'sessions#failure'
  post '/initial_settings', to: 'sessions#initial_settings'
  post '/change_language', to: 'sessions#change_language'
  post '/change_state', to: 'sessions#change_state'
  delete '/logout', to: 'sessions#log_out'
  
  scope module: 'api' do
    get 'api/jobs/search_by_id', to: 'jobs#search_by_id'
    get '/state/:state_id/:state_name/jobs', to: 'jobs#index', as: 'api_jobs'
    get '/api/jobs', to: 'jobs#index', as: 'api_render_jobs'
    get '/api/jobs/autocomplete/where', to: 'jobs#autocomplete_where'
    get '/api/jobs/autocomplete/what', to: 'jobs#autocomplete_what'
    get 'api/business_on_sales/suggestions', to: 'business_on_sales#suggestions'
    get 'api/business_on_sales/salons', to: 'business_on_sales#search'
    post 'api/business_on_sales', to: 'business_on_sales#create'
    get 'api/business_on_sales/cities', to: 'business_on_sales#fetch_cities'
    get 'api/business_on_sales/update_views', to: 'business_on_sales#update_view'
    get 'api/features/jobs', to: 'features#get_featured_jobs'
    get 'api/features/business_on_sales', to: 'features#get_featured_business_on_sales'
    get 'api/used_items/search', to: 'used_items#search'
    get 'api/used_items/suggestions', to: 'used_items#suggestions'
  end

  resources :jobs, only: [:show, :index, :create] do
    collection do
      get '/state/:state_name/:state_id/choose_company', to: 'jobs#choose_company_location', as: 'choose_company_location'
      get '/state/:state_name/:state_id/location/:location_id', to: 'jobs#new', as: 'new'
      
      get '/choose_state', to: 'jobs#choose_state', as: 'choose_state'
    end
  end
  
  resources :business_on_sales, only: [:new, :index]
  resources :states do
    resources :used_items, only: [:index]
  end
  resources :subscriptions, only: :create
  resources :plans, only: [:show] do 
    collection do
      get 'choose_plan', to: 'plans#display_plans', as: 'choose' #choose_plans_path
      get 'choose_business_on_sale_plan', to: 'plans#display_business_on_sale_plans', as: 'choose_business_on_sale' #choose_plans_path
    end
  end

  resources :states, only: [:index] do #state index for yellow page
  	resources :companies, path: :nail_salons, only: [:index, :show, :create] #you can only have a list by states/companies
  end 
  resources :users, only: [] do #for now
  	resources :companies, { path: :nail_salons } #ALL CRUD of companies + company_states happens under USER
  end

  scope module: 'stripe' do
    get '/checkout', to: 'subscriptions#new'
    post '/checkout_callback', to: 'subscriptions#create'
    get 'thank_you', to: 'subscriptions#thank_you', as: 'thank_you'
  end

end
