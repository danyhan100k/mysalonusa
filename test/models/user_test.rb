require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "user calculates tokens left based on payments and jobs" do
    assert_equal 0, user.tokens_left
    Payment.create_and_update_token_for_user(
      user,
      plan,
      "payment_customer_id",
      "payment_charge_id",
      "Test payment",
    )

    assert_equal 10, user.reload.tokens_left

    create_job(user)
    assert_equal 9, user.reload.tokens_left
  end

  test "admin user *always* have 99 tokens left no matter what" do
    user.update!(admin: true)
    assert_equal 77, user.tokens_left

    Payment.create_and_update_token_for_user(
      user,
      plan,
      "payment_customer_id",
      "payment_charge_id",
      "Test payment",
    )

    assert_equal 77, user.reload.tokens_left

    create_job(user)
    assert_equal 77, user.reload.tokens_left
  end

  private

    def user
      @user ||= User.create!(
        provider: "google",
        uid: "000",
        name: "Test User",
        email: "test@user.com",
      )
    end

    def plan
      @plan ||= Plan.create!(
        name: :one_time,
        total_tokens: 10,
        price: "$10.00",
      )
    end

    def create_job(user)
      Job.create!(
        company_location: CompanyLocation.create!(
          company: Company.create!(
            name: "Test Company",
            user: user,
          ),
          address: Address.create!(
            city: City.create!(
              name: "New York",
              state: State.create!(
                name: "NY",
              ),
            ),
          ),
        ),
      )
    end
end
