#What would creating a product be like? 
If product has a blue medium tshirt, would you create 2 ProductVariations with those values?
If that is the case, how can you organize so that those 2 ProductVariations are really one product?
OR, 
do you create each ProductVariation per combo.
ex) 
ProductVariation create product: cool-tshirt, values: [blue, medium]
ProductVariation create product: cool-tshirt, values: [blue, medium]
ProductVariation create product: cool-tshirt, values: [purple, large]

ProductVariation.where(product: cool-tshirt).length == how many cool-tshirts
ProductVariation.where(product: cool-tshirt).map {|x| x.values.map(&:variant)}.flatten.uniq

tshirt = Product.create name: 'cool t-shirt'
ProductVariation.create product: tshirt, values: [blue, medium], in_stock: 9


user 1-* cart
cart 1-* line items
line item *-1 product

cart = Cart.new
user.cart = cart
cart.user == user

class LineItem
  belongs_to :cart
  belongs_to :product_variant

  def price
    product_variant.price * quantity
  end
end

class Cart
  has_many :line_items
  belongs_to :user

  def total
    line_items.map(&:price).sum
  end
end

# product color: red, size: S
# product color: red, size: M
# product color: red, size: L
# product color: blue, size: S
# product color: blue, size: M
# product color: black, size: M

product variants = color X size

# color
class Variant
  has_many :variant_values
end

# variant: color, value: red
# variant: color, value: blue
class VariantValue
  belongs_to :variant
end

class Product
  has_many :product_variants
end

color_variant = Variant.new(name: 'color')
red = VariantValue.new(variant: color_variant, value: 'red')

product_variant = ProductVariant.new
product_variant.variant_values << red

another_product_variant = ProductVariant.new
another_product_variant.variant_values << red

class ProductVariant
  belongs_to :product
  belongs_to :color
  belongs_to :size
  has_and_belongs_to_many :variant_values

  validates :in_stock, numericality: { larger_or_equals_than: 0 }

  delegate :price, to: :product
end

class ApplicationController
  def cart
    return @cart if @cart

    @cart = if current_user
              current_user.cart
            else
              Cart.find_by id: session[:cart_id]
            end

    if @cart.nil?
      @cart = Cart.create(user: current_user)

      unless user
        session[:cart_id] = @cart.id
      end
    end

    @cart
  end
end










---------------------------------------------------------
Products can have many variants, with ALL possible values for each variant,
which means that a user can select any value that's associated with a variant
(for color, it could be either red or blue, independently of the product).

color = Variant.create name: 'color'
size = Variant.create name: 'size'

red = VariantValue.create variant: color, value: 'red'
blue = VariantValue.create variant: color, value: 'blue'

small = VariantValue.create variant: size, value: 'S'
medium = VariantValue.create variant: size, value: 'M'

tshirt = Product.create name: 'T-Shirt'
mug = Product.create name: 'Mug'

ProductVariant.create product: tshirt, variant: color
ProductVariant.create product: tshirt, variant: size

ProductVariant.create product: mug, variant: color

In real life, one model of T-Shirt can have both red and blue variants, and
another model could have only a blue variant available. Besides that, if the
store is NOT on demand, meaning that there's an inventory, a specific variant
can be unavailable at some point, so the user couldn't order it.

-------------------------------------------------------------------
Let's address the first issue:
Now, products are associated with specific *versions* of a variant (a *blue* *small* T-Shirt).

color = Variant.create name: 'color'
size = Variant.create name: 'size'

red = VariantValue.create variant: color, value: 'red'
blue = VariantValue.create variant: color, value: 'blue'
black = VariantValue.create variant: color, value: 'black'

small = VariantValue.create variant: size, value: 'S'
medium = VariantValue.create variant: size, value: 'M'

cool_tshirt = Product.create name: 'Awesome T-Shirt'
boring_tshirt = Product.create name: 'Meh T-Shirt'

ProductVariant.create product: cool_tshirt, value: red
ProductVariant.create product: cool_tshirt, value: blue
ProductVariant.create product: cool_tshirt, value: black
ProductVariant.create product: cool_tshirt, value: small
ProductVariant.create product: cool_tshirt, value: medium

ProductVariant.create product: boring_tshirt, value: blue
ProductVariant.create product: boring_tshirt, value: medium

cool_tshirt.product_variants.map(&:value).map(&:variant).uniq == [color, size]
boring_tshirt.product_variants.map(&:value).map(&:variant).uniq == [color, size]


mug = Product.create name: 'Mug'

ProductVariant.create product: mug, value: blue
ProductVariant.create product: mug, value: black

mug.product_variants.map(&:value).map(&:variant).uniq == [color]

And now, the only piece missing is the inventory.
-----------------------------------------------------
Let's fix that!

color = Variant.create name: 'color'
size = Variant.create name: 'size'

red = VariantValue.create variant: color, value: 'red'
blue = VariantValue.create variant: color, value: 'blue'
black = VariantValue.create variant: color, value: 'black'

small = VariantValue.create variant: size, value: 'S'
medium = VariantValue.create variant: size, value: 'M'

clothing = Category.create name: 'clothing', possible_variants: [color, size]

cool_tshirt = Product.create name: 'Awesome T-Shirt', category: clothing
boring_tshirt = Product.create name: 'Meh T-Shirt', category: clothing

ProductVariantGroup.create product: cool_tshirt, variants: [red, small], count: 3, venue: montreal_store
ProductVariantGroup.create product: cool_tshirt, variants: [red, small], count: 2, venue: ny_store
ProductVariantGroup.create product: cool_tshirt, variants: [red, medium], count: 10
ProductVariantGroup.create product: cool_tshirt, variants: [blue, small], count: 20
ProductVariantGroup.create product: cool_tshirt, variants: [blue, medium], count: 20
ProductVariantGroup.create product: cool_tshirt, variants: [black, small], count: 10
ProductVariantGroup.create product: cool_tshirt, variants: [black, medium], count: 0

cool_tshirt.product_variants.map(&:value).map(&:variant).uniq == [color, size]
boring_tshirt.product_variants.map(&:value).map(&:variant).uniq == [color, size]


mug = Product.create name: 'Mug'

ProductVariant.create product: mug, value: blue
ProductVariant.create product: mug, value: black

mug.product_variants.map(&:value).map(&:variant).uniq == [color]
