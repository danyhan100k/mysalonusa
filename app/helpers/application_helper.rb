module ApplicationHelper
	def pretty_print_state(name)
		if name.present?
			if name.upcase =~ /ILLINOIS/
				name = "chicago"
			end
			return name.split(' ').map(&:capitalize).join(' ')
		else 
			''
		end
	end

	def pretty_print_job_title(title)
		return "" unless title
		title.split(" ").map(&:capitalize).join(" ")
	end

	def remove_new_line_extra_white_space(string)
		string.delete("\n").strip.gsub(/\s+/, " ")
	end


end
