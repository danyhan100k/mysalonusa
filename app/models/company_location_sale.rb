class CompanyLocationSale < ActiveRecord::Base
  belongs_to :company
  belongs_to :address, dependent: :destroy
  has_one :city, through: :address
  has_one :state, through: :city
  has_one :user, through: :company
  has_many :business_on_sales, dependent: :destroy

  accepts_nested_attributes_for :company, :address
end
