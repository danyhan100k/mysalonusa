class Category < ActiveRecord::Base
	has_many :used_items
	validates :name, uniqueness: { case_sensitive: false }
	validates :name, :type_name, presence: true
	enum type_name: { 'UsedItem': 0 }
end
