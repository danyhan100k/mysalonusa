class Payment < ActiveRecord::Base
	belongs_to :user
	belongs_to :plan
	validates :user, :plan, :payment_customer_id, :payment_charge_id, presence: true

	def self.create_and_update_token_for_user(user, plan, stripe_customer_id, stripe_charge_id, description)
    create!(
      user: user, 
      plan: plan, 
      payment_customer_id: stripe_customer_id, 
      payment_charge_id: stripe_charge_id,
      description: description
    )
	end
end
