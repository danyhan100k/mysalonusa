class User < ActiveRecord::Base
	validates_presence_of :provider, :uid
	enum status: { paid: 0, unpaid: 1 }
	has_many :payments
	has_many :business_on_sale_payments
	has_many :business_on_sale_plans, through: :business_on_sale_payments
	has_many :plans, through: :payments
	has_many :exception_trackers
  has_many :companies
  has_many :jobs, through: :companies
  has_many :business_on_sales, through: :companies
  has_many :company_locations, through: :companies
  has_many :company_location_sale, through: :companies
  has_many :used_items
	validates_numericality_of :private_tokens_left, greater_than_or_equal_to: 0, only_integer: true

	def self.from_omniauth(auth_hash)
		user = find_or_create_by!(uid: auth_hash['uid'], provider: auth_hash['provider'])
		user.email = auth_hash['info']['email'] || auth_hash['extra']['raw_info']['email']
		user.name = auth_hash['info']['name']
		user.location = auth_hash['info']['location']
		user.image_url = auth_hash['info']['image']
		user.save
		user
	end

	def tokens_left
    return 77 if admin?
		plans.sum(:total_tokens).to_i - jobs.count
	end

	def business_on_sale_tokens_left
		return 77 if admin?
		business_on_sale_plans.sum(:total_tokens).to_i -  business_on_sales.count
	end

	def update_status_to(n)
		status = n
		save
	end

end
