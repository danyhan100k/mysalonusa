class Address < ActiveRecord::Base
  has_one :company_location
  belongs_to :city
  has_one :state, through: :city
  validates_presence_of :city
  accepts_nested_attributes_for :city
  def full_address
    [address_line_1, address_line_2, city.name, state.name, city.zip].join(", ")
  end
end
