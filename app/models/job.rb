class Job < ActiveRecord::Base
  searchkick merge_mappings: true, mappings: {
    job: {
      properties: {
        created_at: {
          type: 'date'
        }
      }
    }
  }
 # searchkick merge_mappings: true, mappings: {job:
 #  {"_all"=>{"analyzer"=>"searchkick_text_start_index"},
 #   "dynamic_templates"=>
 #    [{"string_template"=>
 #       {"match"=>"*",
 #        "match_mapping_type"=>"string",
 #        "mapping"=>{"fields"=>{"analyzed"=>{"index"=>"analyzed", "type"=>"text"}}, "ignore_above"=>256, "include_in_all"=>true, "type"=>"string", "index" => "not_analyzed" }}}],
 #   "properties"=>
 #    {"address"=>
 #      {"properties"=>
 #        {"address_line_1"=>{"type"=>"string", "index" => "not_analyzed", "fields"=>{"analyzed"=>{"type"=>"text"}}, "include_in_all"=>true, "ignore_above"=>256},
 #         "address_line_2"=>{"type"=>"string", "index" => "not_analyzed", "fields"=>{"analyzed"=>{"type"=>"text"}}, "include_in_all"=>true, "ignore_above"=>256},
 #         "city"=>
 #          {"properties"=>
 #            {"name"=>{"type"=>"string", "index" => "not_analyzed", "fields"=>{"analyzed"=>{"type"=>"text"}}, "include_in_all"=>true, "ignore_above"=>256},
 #             "state"=>
 #              {"properties"=>
 #                {"abbreviation"=>{"type"=>"string", "index" => "not_analyzed", "fields"=>{"analyzed"=>{"type"=>"text"}}, "include_in_all"=>true, "ignore_above"=>256},
 #                 "name"=>{"type"=>"string", "index" => "not_analyzed",  "fields"=>{"analyzed"=>{"type"=>"text"}}, "include_in_all"=>true, "ignore_above"=>256}}},
 #             "zip"=>{"type"=>"string", "index" => "not_analyzed", "fields"=>{"analyzed"=>{"type"=>"text"}}, "include_in_all"=>true, "ignore_above"=>256}}}}},
 #     "company"=>{"properties"=>{"name"=>{"type"=>"string", "index" => "not_analyzed", "fields"=>{"analyzed"=>{"type"=>"text"}}, "include_in_all"=>true, "ignore_above"=>256}}},
 #     "created_at"=>{"type"=>"date"},
 #     "description"=>{"type"=>"string", "index" => "not_analyzed", "fields"=>{"analyzed"=>{"type"=>"text"}}, "include_in_all"=>true, "ignore_above"=>256},
 #     "job_id"=>{"type"=>"long"},
 #     "job_type"=>{"type"=>"string", "index" => "not_analyzed", "fields"=>{"analyzed"=>{"type"=>"text"}}, "include_in_all"=>true, "ignore_above"=>256},
 #     "title"=>{"type"=>"string", "index" => "not_analyzed", "fields"=>{"analyzed"=>{"type"=>"text"}}, "include_in_all"=>true, "ignore_above"=>256},
 #     "views"=>{"type"=>"long"}}},
 # _default_:
 #  {"_all"=>{"analyzer"=>"searchkick_text_start_index"},
 #   "dynamic_templates"=>
 #    [{"string_template"=>
 #       {"match"=>"*",
 #        "match_mapping_type"=>"string",
 #        "mapping"=>{"fields"=>{"analyzed"=>{"index"=>"analyzed", "type"=>"text"}}, "ignore_above"=>256, "include_in_all"=>true, "type"=>"string", "index" => "not_analyzed" }}}]}}
  belongs_to :company_location
  has_one :address, through: :company_location
  has_one :state, through: :company_location
  has_one :company, through: :company_location
  has_one :user, through: :company_location
  has_one :feature, as: :featureable
  has_and_belongs_to_many :languages
  enum job_type: [:'full time', :'part time', :contract]
  validate :set_created_at_reformat #ToDo: how it is used
  validates :languages, presence: { message: "Please choose language"}
  validates_length_of :job_title, maximum: 26, allow_blank: false
  accepts_nested_attributes_for :company_location
  CategoryCreator = Struct.new :name, :list

  def self.set_languages(job, params) 
    keys = params.keys.select {|key| key.include? "language"}
    languages = keys.map {|key| Language.find(params[key])}
    job.languages = languages
    job
  end

  def self.featured_jobs_for_state(state_id)
    return [] if state_id.nil?
    joins = "INNER JOIN features ON (jobs.id = features.featureable_id AND features.featureable_type = 'Job') 
             INNER JOIN company_locations ON (jobs.company_location_id = company_locations.id) 
             INNER JOIN addresses ON (addresses.id = company_locations.address_id) 
             INNER JOIN cities ON (cities.id = addresses.city_id)"

    self.joins(joins)
        .where("cities.state_id = :state_id AND 
                features.created_at >= :created_at", 
                state_id: state_id, created_at: Feature::EXPIRATION_DATE)
        .order("random()")
  end

  def self.length_of_featured_jobs_for_state(state_id)
    joins = "INNER JOIN features ON (jobs.id = features.featureable_id AND features.featureable_type = 'Job') 
             INNER JOIN company_locations ON (jobs.company_location_id = company_locations.id) 
             INNER JOIN addresses ON (addresses.id = company_locations.address_id) 
             INNER JOIN cities ON (cities.id = addresses.city_id)"
    self.joins(joins)
        .where("cities.state_id = :state_id AND 
                features.created_at >= :created_at", 
                state_id: state_id, created_at: Feature::EXPIRATION_DATE)
        .length
  end

  def self.search_by_id_for_modal(id)
    job = Job.includes(:company_location => [:company, :address => [ :city => :state]])
             .where('jobs.id = ?', id)
             .first
    job.views += 1
    job.save
    return job.as_json(except: 
      [:id, :updated_at, :created_at, :created_at_reformat], 
      include: {
        company: {
          only: [:name]
        },
        address: {
          only: [:address_line_1, :address_line_2],
          include: {
            city: {
              only: [:name, :zip],
              include: {
                state: {
                  only: [:name]
                }
              }
            }
          }
        }
      }).merge({ created_at_utc: job.created_at.utc.to_s })
  end

  def self.autocomplete_what(value)
    list = Set.new
    self.search(value, {
      match: :word_start,
      load: false
    }).map do |job|
      list << job.title
      list << job.company.name
      list << job.job_type
      job[:languages][:languages].each {|l| list << l }
    end
    return list.select(&:present?).select {|x| x.upcase.include? value.upcase}
  end

  def self.autocomplete_where(value)
    list = []
    self.search(value, {
      match: :word_start,
      load: false
    }).map do |job|  
      state = job.address.city.state.name
      city = job.address.city.name
      zip = job.address.city['zip']
      state_abbrev = job.address.city.state.abbreviation

      list << state unless list.include? state
      list << city unless list.include? city
      list << zip unless list.include? zip
      list << state_abbrev unless list.include? state_abbrev
    end
    return list.compact.select {|x| x.upcase.include? value.upcase}
    # self.search(value, {
    #   match: :word_start,
    #   load: false,
    # }).map {|job| job.address.city.state.name }.uniq
  end

  def search_data
    {
      job_id: id,
      title: title,
      description: description,
      job_type: job_type,
      address: {
        address_line_1: address.address_line_1,
        address_line_2: address.address_line_2,
        city: {
          name: address.city.name,
          zip: address.city.zip,
          state: {
            name: address.city.state.name,
            abbreviation: address.city.state.abbreviation
          }
        },
      },
      company: {
        name: company.name
      },
      languages: { languages: languages.map(&:name) },
      views: views,
      created_at: created_at.to_i
    }
  end

  def self.stringify_job_ids(job_ids)
    string_ids = ""
    job_ids.each {|id| string_ids << "#{id}," }
    string_ids = string_ids.split(',').join(',')
  end

  def self.create_main_page_categories(state_id)
    cities_id = State.find(state_id).cities.pluck(:id).uniq
    sql = City.joins(:addresses => [:company_location => :jobs])
        .where(:"cities.id" => cities_id)
        .select("cities.name, COUNT(*)")
        .group("cities.name").to_sql

    result = ActiveRecord::Base.connection.execute(sql).to_a
    result_with_type = []
    result.each do |r|
      result_with_type.push(r.merge({ type: 'location' }))
    end
    location_category = CategoryCreator.new('Location', result_with_type)

    sql = Job.joins(:company_location => [:address => [:city => :state]])
       .where('states.id = ?', state_id)
      .select("jobs.job_type, COUNT(*)")
      .group("jobs.job_type")
      .to_sql

    result = ActiveRecord::Base.connection.execute(sql).to_a
    result_with_type = []
      result.each do |group|
        type_name = Job.job_types.invert[group['job_type'].to_i].split(' ').map(&:capitalize).join(' ')
        result_with_type << { 'name' => type_name, 'count' => group['count'], type: 'job_type'}
     end
    job_type_category = CategoryCreator.new('Job Type', result_with_type)
    result = nil
    [location_category, job_type_category]
  end

  def self.group_by_location(job_ids)
    sql = "SELECT \"cities\".\"name\", COUNT(*) FROM \"jobs\" INNER JOIN 
          \"company_locations\" ON \"company_locations\".\"id\" = \"jobs\".\"company_location_id\" 
          INNER JOIN \"addresses\" ON \"addresses\".\"id\" = \"company_locations\".\"address_id\" 
          INNER JOIN \"cities\" ON \"cities\".\"id\" = \"addresses\".\"city_id\" 
          WHERE \"jobs\".\"id\" IN (#{Job.stringify_job_ids(job_ids)}) 
          GROUP BY \"cities\".name"
    result = ActiveRecord::Base.connection.execute(sql).to_a
    result_with_type = []
    result.each do |r|
      result_with_type.push(r.merge({ type: 'location' }))
    end
    result = nil
    CategoryCreator.new('Location', result_with_type)
  end

  def self.group_by_company(job_ids)
    sql = "SELECT \"companies\".\"name\", COUNT(*) FROM \"jobs\" INNER JOIN 
        \"company_locations\" ON \"company_locations\".\"id\" = \"jobs\".\"company_location_id\" 
        INNER JOIN \"companies\" ON \"companies\".\"id\" = \"company_locations\".\"company_id\"
        WHERE \"jobs\".\"id\" IN (#{Job.stringify_job_ids(job_ids)}) 
        GROUP BY \"companies\".name"
    result = ActiveRecord::Base.connection.execute(sql).to_a
    result_with_type = []
    result.each do |r|
      result_with_type.push(r.merge({ type: 'company' }))
    end
    result = nil
    CategoryCreator.new('Company', result_with_type)
  end

  def self.group_by_jobtype(job_ids)
    sql = "SELECT \"jobs\".\"job_type\", COUNT(*) FROM \"jobs\"
          WHERE \"jobs\".\"id\" IN (#{Job.stringify_job_ids(job_ids)}) 
          GROUP BY \"jobs\".job_type"
    result = ActiveRecord::Base.connection.execute(sql).to_a
    new_list = []
    result.each do |group|
      type_name = Job.job_types.invert[group['job_type'].to_i].split(' ').map(&:capitalize).join(' ')
      new_list << { 'name' => type_name, 'count' => group['count'], type: 'job_type'}
    end
    result = nil
    CategoryCreator.new('Job Type', new_list)
  end

  def set_created_at_reformat
  	if self.created_at_reformat.blank? && self.created_at.present?
  		self.created_at_reformat = self.created_at.strftime("%Y-%m-%d")
  	end
  end

  def reset_created_at_reformat
    self.created_at_reformat = self.created_at.strftime("%Y-%m-%d")
  end

  def self.test_search1(value, page, per_page)
    Job.search(value, page: page, per_page: per_page, operator: "or", order: {created_at: :desc})
       .records.to_a.map {|x| [x.address.city.name, x.address.city.state.name]}
  end

  def self.test_search2(value, page, per_page)
    Job.search(value, page: page, per_page: per_page, operator: "or", order: {created_at: :desc})
       .records.to_a.map {|x| { job_title: x.title, company_name: x.company.name }}
  end
end
