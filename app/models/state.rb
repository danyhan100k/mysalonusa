class State < ActiveRecord::Base
	has_many :cities, dependent: :destroy
	has_many :addresses, through: :cities
	has_many :company_locations, through: :addresses
	has_many :jobs, through: :company_locations
	before_create :lower_case_name
	validates :name, { presence: true, uniqueness: { message: "state already exists"} }
	before_save :capitalize_columns

	def capitalize_columns
		self.abbreviation = self.abbreviation.upcase if self.abbreviation.present?
		self.name = self.name.capitalize if self.name.present?
	end
	def lower_case_name
		self.name = self.name.downcase
	end
	
	def self.find_ny
		State.find_by("upper(name) LIKE upper('%york')")
	end
	def self.find_il
		State.find_by("upper(name) LIKE upper('illi%')")
	end
	def self.find_cali
		State.find_by("upper(name) LIKE upper('cali%')")
	end
end
