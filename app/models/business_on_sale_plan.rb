class BusinessOnSalePlan < ActiveRecord::Base
	  monetize :price_cents
	  enum name: { 'Single Pay': 0, 'Group Package': 1}
	  has_many :business_on_sale_payments
	  has_many :users, through: :business_on_sale_payments
end
