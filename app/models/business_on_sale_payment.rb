class BusinessOnSalePayment < ActiveRecord::Base
  belongs_to :user
  belongs_to :business_on_sale_plan
 	validates :user, :business_on_sale_plan, :payment_customer_id, :payment_charge_id, presence: true

 	def self.create_and_update_token_for_user(user, plan, stripe_customer_id, stripe_charge_id, description)
    create!(
      user: user, 
      business_on_sale_plan_id: plan.id, 
      payment_customer_id: stripe_customer_id, 
      payment_charge_id: stripe_charge_id,
      description: description
    )
	end
end
