class Plan < ActiveRecord::Base
  monetize :price_cents
	enum name: { Standard: 0, Gold: 1, Platinum: 2}
	validates :name, presence: true
	validates :name, uniqueness: { case_sensitive: false } 
end
