class Language < ActiveRecord::Base
	validates :name, presence: true, uniqueness: true
	has_and_belongs_to_many :jobs

	def self.all_for_buttons
		colors = ["orange", "blue", "violet", "purple", "yellow"]
		all.map {|l| { id: l.id, name: l.name, color: colors.sample }}
	end
end
