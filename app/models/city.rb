class City < ActiveRecord::Base
	belongs_to :state
	has_many :addresses, dependent: :destroy
	validates :state, :name, presence: true
	accepts_nested_attributes_for :state
	before_save :capitalize_columns

	def self.find_all_by_state(state)
		state = State.where("upper(name) LIKE :state", state: "%#{state.upcase}%").first
		return state.cities.order("name ASC").pluck(:name).uniq
	end

	def capitalize_columns
		self.name = self.name.capitalize if self.name.present?
	end
end
