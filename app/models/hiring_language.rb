class HiringLanguage < ActiveRecord::Base
	searchkick merge_mappings: true, mappings: {
    hiring_language: {
      properties: {
        created_at: {
          type: 'date'
        }
      }
    }
  }

  belongs_to :job
end
