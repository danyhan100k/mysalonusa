class CompanyLocation < ActiveRecord::Base
  belongs_to :company
  belongs_to :address, dependent: :destroy
  has_one :city, through: :address
  has_one :state, through: :city
  has_one :user, through: :company
  has_many :jobs, dependent: :destroy
  accepts_nested_attributes_for :company, :address

  def self.find_by_user_and_state(user_id, state_id)
#TODO: find out if this is eager loading in the log
    company_locations = CompanyLocation.joins(:address => { :city => :state }, :company => :user)
                                       .distinct
                                       .where('users.id = :user_id AND states.id = :state_id', user_id: user_id, state_id: state_id)                                       
    return company_locations.includes(company: [:user], address: [city: [:state]])
  end

  def name
    "#{company.name} - #{address.full_address}"
  end
end
