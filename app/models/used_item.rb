class UsedItem < ActiveRecord::Base
	searchkick word_start: [
		:title, :city_name, :city_zip, :state_name,
		:state_abbreviation, :price, :category
	], 
	merge_mappings: true, mappings: {
    used_item: {
      properties: {
        created_at: {
          type: 'date'
        }
      }
    }
  }
  belongs_to :user
  belongs_to :city
  belongs_to :category
  has_one :state, through: :city
  has_many :phone_numbers, as: :phoneable
  monetize :price_cents
  validates :user, :city, :category, :title, :description, :contact_email, presence: true
  mount_uploaders :images, ImageUploader

  SEARCH_FIELDS = [
  	:title, :city_name, :city_zip, :state_name, 
  	:state_abbreviation, :price, :category
 	]
  PER_PAGE = 20
  def self.search_for(value, page=1)
  	result = self.search(
      value,
      load: false,
      fields: SEARCH_FIELDS,
      page: page,
      per_page: PER_PAGE,
      order: { created_at: :desc },
      operator: "or"
    )
    client = {
      usedItems: result.map {|item| item},
      length: result.length,
      currentPage: result.current_page,
      perPage: result.per_page,
      totalPages: result.total_pages,
      totalEntries: result.total_entries,
      searchedValue: value
    }
    result = nil
    return client
  end

  def self.autosuggest(value)
    list = Set.new
    search_fields = [
      :title, :city_name, :city_zip, :state_name, 
      :state_abbreviation, :category
    ]
    self.search(
      value, 
      load: false, 
      fields: search_fields, 
      match: :word_start
    ).map do |item|
      list << item.title
      list << item.city_name
      list << item.city_zip
      list << item.state_name
      list << item.state_abbreviation
      list << item.category
    end
    return list.select(&:present?)
               .select {|x| x.upcase.include? value.upcase }
  end

  def search_data
  	{
  		id: id,
  		title: title,
  		description: description,
  		email: contact_email,
  		city_name: city.name.capitalize,
  		city_zip: city.zip,
  		state_name: state.name.capitalize,
  		state_abbreviation: state.abbreviation.upcase,
  		price: (!price.zero? ? price.to_money.format : nil),
  		images: images.map {|x| x.url },
  		category: category.name.capitalize,
      phone_numbers: phone_numbers.map(&:number),
      views: views,
      created_at: created_at.to_i,
      created_at_utc: created_at.utc.to_s
  	}
  end

end
