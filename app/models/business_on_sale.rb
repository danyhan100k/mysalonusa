class BusinessOnSale < ActiveRecord::Base
  searchkick word_start: [ :title, :address_line_1, 
    :address_line_2, :city_name, :city_zip, 
    :state_name, :state_abbreviation, 
    :company_name
  ], 
  merge_mappings: true, mappings: {
    business_on_sale: {
      properties: {
        created_at: {
          type: 'date'
        }
      }
    }
  }
  belongs_to :company_location_sale
  has_one :address, through: :company_location_sale
  has_one :city, through: :company_location_sale
  has_one :state, through: :company_location_sale
  has_one :company, through: :company_location_sale
  has_one :user, through: :company
  has_one :feature, as: :featureable
  accepts_nested_attributes_for :company_location_sale
  validates_presence_of :title, :description, :contact_email
  monetize :price_cents 
  mount_uploaders :images, ImageUploader
  before_save :capitalize_columns

  def self.update_view_for(id)
    business = BusinessOnSale.find(id)
    business.views += 1
    business.save
  end

  def capitalize_columns
    self.title = self.title.capitalize if self.title.present?
    self.description = self.description.capitalize if self.description.present?
  end

  SEARCH_FIELDS= [:title, :address_line_1, 
      :address_line_2, :city_name, :city_zip,
       :state_name, :state_abbreviation, :company_name ]
  PER_PAGE=20

  def self.search_for(value, page=1)
    result = self.search(
      value,
      load: false,
      fields: SEARCH_FIELDS,
      page: page,
      per_page: PER_PAGE,
      order: { created_at: :desc },
      operator: "or"
    )
    client = {
      salons: result.map {|salon| salon},
      length: result.length,
      currentPage: result.current_page,
      perPage: result.per_page,
      totalPages: result.total_pages,
      totalEntries: result.total_entries,
      searchedValue: value
    }
    result = nil
    return client;
  end

  def self.autosuggest(value)
    list = Set.new

    self.search(
      value, 
      load: false, 
      fields: SEARCH_FIELDS, 
      match: :word_start
    ).map do |salon|
      list << salon.title
      list << salon.address_line_1
      list << salon.address_line_2
      list << salon.city_name
      list << salon.city_zip
      list << salon.state_name
      list << salon.state_abbreviation
      list << salon.company_name
    end
    return list.select(&:present?)
               .select {|x| x.upcase.include? value.upcase }
  end

  def search_data
    {
      business_on_sale_id: id,
      title: title,
      description: description,
      email: contact_email,
      address_line_1: address.address_line_1,
      address_line_2: address.address_line_2,
      city_name: city.name,
      city_zip: city.zip,
      state_name: state.name,
      state_abbreviation: state.abbreviation,
      price: (!price.zero? ? price.to_money.format : nil),
      company_name: company.name,
      images: images.map {|x| x.url },
      views: views,
      created_at: created_at.to_i,
      created_at_utc: created_at.utc.to_s
    }
  end

  def as_json(options)
    super(:only => [:title, :description, :contact_email, :images, :created_at], 
          include: [
            :company => { 
              only: [ :name ], 
              include: [
                :user => { 
                  only: [:email] 
                } 
              ]
            },
            :address => {
              only: [:address_line_1, :address_line_2],
              include: [
                :city => {
                  only: [:name, :zip],
                  include: [ 
                    :state => {
                      only: [:name]
                    }
                  ]
                }
              ]
            }
          ]
    ).merge({ price: self.price.format, created_at_utc: created_at.utc.to_s })
  end 

end
