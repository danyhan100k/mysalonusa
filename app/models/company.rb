class Company < ActiveRecord::Base
	belongs_to :user
	has_many :company_locations, dependent: :destroy
  has_many :company_location_sales, dependent: :destroy
  has_many :jobs, through: :company_locations
  has_many :business_on_sales, through: :company_location_sales
  # TODO: Remove duplicates
	has_many :states, through: :company_locations
  validates_presence_of :name

  # TODO: Enforce validation below
  # A company must have an unique name for each user. It's ok to
  # have duplicate name as long as the companies belong to
  # different users.
  after_save :update_business_on_sale_and_job
  before_save :capitalize_columns

  def capitalize_columns
    self.name = self.name.capitalize if self.name.present?
  end

  def update_business_on_sale_and_job
    if self.jobs.any? 
      self.jobs.each {|job| job.save}
    end
    if self.business_on_sales.any?
      self.business_on_sales.each {|sale| sale.save}
    end
  end

end
