class Feature < ActiveRecord::Base
	MAX = 16
  EXPIRATION_DATE = 200.days.ago
  belongs_to :featureable, polymorphic: true
  default_scope -> { where("created_at >= ?", EXPIRATION_DATE) } 
  scope :random, -> { order('random()') }
  scope :of_type, -> (type) { 
    where(featureable_type: type) 
  }
  scope :jobs, -> { of_type("Job").random }
  scope :business_on_sales, -> {
  	of_type("BusinessOnSale").random
  }
  validate :max_feature, on: :create

  def expires_at 
  	created_at + 30.days
  end

  private
 	def max_feature
    if self.featureable_type = 'Job'
      state_id = self.featureable.state.id
   		if Job.length_of_featured_jobs_for_state(state_id) >= MAX
   			errors.add(:id, "Limit reached for Feature type #{featureable_type} for state #{state_id}")
   		end
    end
 	end

end
