class BusinessOnSalesController < ApplicationController
	before_action :require_sign_in, only: [:new]
	before_action :check_user_business_on_sale_tokens, only: [:new]

	def new 
	end

	def index
		@initial_props = { initialSearch: cookies['state_name'] }
	end
end
