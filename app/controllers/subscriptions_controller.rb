class SubscriptionsController < ApplicationController
	#this is for email subscriptions on footer
	def create
		@subscription = Subscription.new(email: email)
		if @subscription.save
			render json: @subscription, status: 200
		else
			render json: { errors: @subscription.errors.full_messages, status: 400 }
		end
	end

	private
	def email
		params[:email]
	end

end
