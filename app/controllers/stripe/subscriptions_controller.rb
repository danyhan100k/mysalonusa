class Stripe::SubscriptionsController < ApplicationController
	skip_before_action :verify_authenticity_token
	# before_action :authenticate_user!

	def new
	end

	def create
		#NOTICE: Always expecting session[:intended_url]
		begin
			StripeTool.create_payment(
				amount_to_be_charged: amount_to_be_charged,
				stripe_email: params[:stripeEmail],
				stripe_token: token,
				description: description,
				user: current_user,
				plan: plan,
				type: type
			)
			intended_url = remove_intended_url
			redirect_to intended_url, notice: "Purchased #{plan.total_tokens} Tokens (Posts)"
		rescue => e			
			ExceptionTracker.create(location: "SELF: #{self}, Payment.create_and_update_token_for_user",
															description: e.exception, 
															backtrace: e.backtrace)
			flash[:alert] = "Card has been declined, #{e.message}"
			redirect_to session[:intended_url]
		end
	end

	private
	def token
		@token ||= params[:stripe_token]
	end

	def plan
		if type == 'business_on_sale'
			return BusinessOnSalePlan.find(params[:plan_id])
		end
		if type == 'job'
			return Plan.find(params[:plan_id])
		end
	end

	def amount_to_be_charged
		@amount ||= plan.price_cents
	end

	def current_time
		Time.now.strftime("%m/%d/%Y %H:%M")
	end

	def description
		@description ||= "Paid for #{type} Plan #{plan.name} - #{plan.price.format} at #{current_time}"
	end

	def type
		@type ||= params[:type]
	end

end
