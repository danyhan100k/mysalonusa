class Api::JobsController < ApplicationController
	before_action :set_state_name
	def index 
		respond_to do |format|
			format.html do 
        if request.xhr? 
        	@jobs = JobsSearchAdapter.new(params).search
        	set_categories(@jobs)
          render partial: 'api/jobs/jobs', layout: false, locals: { jobs: @jobs }
        else 
        	@jobs = JobsSearchAdapter.new(params).search
        	set_categories(@jobs)
        	set_search_bar_values
        end
			end
			format.json do 
				render json: JobsSearchAdapter.new(params)
			end 
		end
	end
	
	def search_by_id
		render json: Job.search_by_id_for_modal(params[:id])
	end

	def autocomplete_what
		render json: Job.autocomplete_what(params[:searchValue])		
	end

	def autocomplete_where
		render json: Job.autocomplete_where(params[:searchValue])
	end

	private
	def set_search_bar_values 
		@addressSearch = params["addressSearch"]
		@job_company_search = params["jobCompanySearch"]
	end
end
