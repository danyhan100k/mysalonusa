class Api::UsedItemsController < ApplicationController
	def search
		render json: UsedItem.search_for(params[:search_value], params[:page])
	end

	def suggestions
		respond_to do |format|
			format.json do 
				render json: UsedItem.autosuggest(params[:suggestion])
			end
		end
	end

end
