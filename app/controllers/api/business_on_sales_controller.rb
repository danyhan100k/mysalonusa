class Api::BusinessOnSalesController < ApplicationController
	def suggestions
		respond_to do |format|
			format.json do 
				render json: BusinessOnSale.autosuggest(params[:suggestion])
			end
		end	
	end

	def fetch_cities
		cities = City.find_all_by_state(params[:state])
		render json: { city_attributes: cities }
	end

	def search 
		render json: BusinessOnSale.search_for(
										params[:search_value], 
										params[:page]
								 )
	end

	def create
    # TODO:
    # - Fix BusinessOnSale#save (right now it's broken due to #search_data)
    # - Finish writing this action AND connect this to the callback on the front-end side
    # - Refactoring the reduce into NOT performing ajax requests or dispatching actions (use action creators for that)
        business_on_sale_params = JSON.parse(params['business_on_sale'])
        new_params = business_on_sale_params['job'].merge({ 
        	company_location_sale_attributes: { 
        		address_attributes: business_on_sale_params['address_attributes'], 
        		company_attributes: business_on_sale_params['company_attributes'] 
        	} 
        }).merge({images: params['images']})

        @business_on_sale = BusinessOnSale.new(new_params)
        @business_on_sale.company_location_sale.company.user = current_user 
        @business_on_sale.company_location_sale.address.city = City.where('UPPER(name) LIKE UPPER(?)', "%#{business_on_sale_params['city']}%").first
        
        if business_on_sale_params['job']['price'].nil? || business_on_sale_params['job']['price'].blank? || business_on_sale_params['job']['price'] =~ /[a-zA-Z]/
        	@business_on_sale.price = 0
        end

        if @business_on_sale.save
        	render json: { 
        		createdSalon: @business_on_sale
        	}, status: 200
        else
        	render json: {
        		error: @business_on_sale.errors
        	}, status: :unprocessable_entity
        end
	end

    def update_view 
        BusinessOnSale.update_view_for(params[:salon_id])
        render :nothing => true, status: 200, :content_type => 'json'
    end
end
