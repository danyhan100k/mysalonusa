class PlansController < ApplicationController
	before_action :require_sign_in, only: [:display_plans, :display_business_on_sale_plans]

	def display_plans  #for jobs
		#NOTICE: Always expecting session[:intended_url]
		redirect_to root_path if session[:intended_url].blank?
		@plans = Plan.all
	end

	def display_business_on_sale_plans
		redirect_to root_path if session[:intended_url].blank?
		@business_on_sale_plans = BusinessOnSalePlan.all
	end
end
