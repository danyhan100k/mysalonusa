class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  after_action :set_current_user_cookie
  before_filter :set_nav_menu_props
  before_action :set_locale

  private
  helper_method :current_user

  def set_locale
    I18n.locale = cookies[:locale]
  end

  def set_nav_menu_props
    menu_props = { 
      state_name: cookies[:state_name],
      state_id: cookies[:location]
    }
    if current_user
      menu_props.merge!({ 
        current_user: current_user.email,
        tokens_left: current_user.tokens_left,
        business_post_left: current_user.business_on_sale_tokens_left
      })
    end

    @nav_menu_app_props = {
      content: menu_props
    }
    return @nav_menu_app_props;
  end

  def require_sign_in
    unless current_user
      remove_intended_url
      session[:intended_url] = request.env['REQUEST_PATH']
      redirect_to sign_in_path
    end
  end 

  def set_intended_url
    remove_intended_url
    session[:intended_url] = request.env['REQUEST_PATH']
  end

  def remove_intended_url
    return session.delete(:intended_url)
  end

  def current_user
    if session[:user_id].nil?
      return @current_user = nil
    end
  	@current_user ||= User.find_by(id: session[:user_id])
  end

  def set_current_user_cookie
    cookies[:current_user_id] = current_user.try(:id)
  end

  def set_state_name
    @state_name = State.find_by(id: cookies['location']).try(:name)
    if @state_name.blank?
      cookies['location'] = nil
    end
    params['location'] = @state_name
  end

  def set_main_page_categories
    return nil unless cookies[:location]
    state_id = cookies[:location]
    @categories = Job.create_main_page_categories(state_id)
  end

  def set_categories(jobs)
    return @categories = [] if !jobs.any?
    job_ids = jobs.records.pluck(:id)
    location_category = Job.group_by_location(job_ids)
    # company_category = Job.group_by_company(job_ids)
    jobtype_category = Job.group_by_jobtype(job_ids)
    @categories = [location_category, jobtype_category]
  end

  def access_denied(*)
    flash[:alert] = "Access denied"
    redirect_to root_path
  end

  def check_user_tokens
    if current_user.tokens_left < 1
      set_intended_url
      flash[:alert] = "You don't have enough tokens, please purchase more"
      return redirect_to choose_plans_path
    end
  end
  def check_user_business_on_sale_tokens
    if current_user.business_on_sale_tokens_left < 1
      set_intended_url
      flash[:alert] = "You don't have enough tokens, please purchase more"
      return redirect_to choose_business_on_sale_plans_path
    end
  end
end
