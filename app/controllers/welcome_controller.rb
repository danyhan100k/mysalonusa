class WelcomeController < ApplicationController
  before_action :set_state_name
  before_action :set_main_page_categories, only: :index
  def index
		@jobs = JobsSearchAdapter.new(params.merge({ length: 10}))
														 .search.records
		if @jobs.any?
				@jobs = @jobs.includes(:company_location => [:company, :address => :city])
										 .order("created_at DESC")
		end
		@featured_jobs = Job.featured_jobs_for_state(cookies['location'])
		if @featured_jobs.any?
				@featured_jobs = @featured_jobs.includes(:company_location => :company)
		end
	end

	def coming_soon
	end

	private
	def state
		@state ||= State.find_by(id: cookies['location'])
	end 

	def param
		{ state: { abbreviation: state && state.abbreviation }}
	end 
end
