class JobsController < ApplicationController
	before_action :require_sign_in, only: [:choose_company, :choose_company_location, :create, :new]
	#before_action :check_user_tokens, only: [:new, :create] FREE NOW 

	def index
	end

  def show 
    @job = Job.find(params[:id])
    @job.views += 1
    @job.save
    @job = Job.preload(:company_location => [:company, {:address => {:city => :state} }]).where('jobs.id = ?', params[:id]).first
  end

  def new
    @job = Job.new
    @company_location = params[:location_id]
    @languages = Language.all_for_buttons
    setup_new_job_form
  end

  def create
    @job = Job.new(job_params)
    @job = Job.set_languages(@job, params)
    @job.company_location.company.user = current_user
    if @job.save
      state = @job.state
      cookies[:location] = state.id
      cookies[:state_name] = state.name
      flash[:notice] = "Job successfully created! with id: #{@job.id} Please Refresh this page"
      redirect_to api_jobs_path(state.id, state.name)
    else      
      flash[:alert] = @job.errors.full_messages.join(', ')
      setup_new_job_form
      @company_locations = @job.company_location.user.company_locations
      @state_name = @job.state.name 
      @state_id = @job.state.id
      render :choose_company_location
    end


  #CASE 2) Only creating Job
    # @job = Job.new(job_params)

    # This is necessary because we are *not* redirect back to the
    # choose_company action. Rather, we're just *rendering* its view
    # again, but the @job instance variable is set above. The @job
    # must have a company_location populated otherwise it won't
    # produce any HTML for the nested company_location form. The
    # same applies for the address and the company, which are nested
    # under company_location.
    # Remove it when we have the company_location_id.
    # @job.company_location.build_address unless @job.company_location.address
    # @job.company_location.build_company unless @job.company_location.company

    # @job.company_location.company.user = current_user
  end
	
	def choose_state
    @new_york = State.find_ny
    @california = State.find_cali
    @illinois = State.find_il
	end
  
  def choose_company_location 
    @company_locations = CompanyLocation.find_by_user_and_state(current_user.id, params[:state_id])
    if @company_locations.blank?
      redirect_to new_jobs_path(state_name: params[:state_name], 
                                state_id: params[:state_id], 
                                location_id: :new)
    end
  end

	private	

  def job_params
    params.require(:job).permit(
      :title,
      :job_title,
      :description,
      :job_type,
      :contact_email,
      :company_location_id,
      company_location_attributes: [
        :company_id,
        company_attributes: [:name],
        address_attributes: [
          :address_line_1,
          :address_line_2,
          :city_id
        ]
      ]
    )
  end

  def setup_new_job_form
    @state = State.find(params[:state_id] || params[:job][:state_id])
    if params[:location_id] == 'new'
      @job.build_company_location   
      @job.company_location.build_company
      @job.company_location.build_address
    else
      @company_location = CompanyLocation.find(params[:location_id] || params[:job][:company_location_id])
    end
  end
end
