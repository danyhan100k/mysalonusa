class SessionsController < ApplicationController
	def sign_in
		#any redirect to this path REQUIRES session[:intended_url]
		#DELETE raise FOR Production
		begin
			raise 'Need to provide session[:intended_url]' if session[:intended_url].blank?
			@intended_url = remove_intended_url
		rescue => e
			ExceptionTracker.create(location: "SELF: #{self}, SessionsController#sign_in",
																description: e.exception,
																backtrace: e.backtrace)
			redirect_to root_path
		end
	end

	def callback 		
		begin
			@user = User.from_omniauth(request.env['omniauth.auth'])
			session[:user_id] = @user.id
			flash[:notice] = "Welcome, #{@user.name}!"
		rescue Exception => e
			flash[:alert] = "There was an error while signing in please Message us in Chat..."
		end
		redirect_to params[:redirect_to]
	end

	def failure
		@error_msg = request.params['message']
	end

  def initial_settings
    cookies[:locale] = params[:locale]
    cookies[:location] = find_state_id
    cookies[:state_name] = State.find(find_state_id).name
    redirect_to :back
  end

  def change_language
		cookies[:locale] = params[:locale]
		redirect_to :back
  end

  def change_state
    cookies[:location] = find_state_id
    cookies[:state_name] = State.find(find_state_id).name
    redirect_to :back
  end

  def log_out
  	if current_user
  		session.delete(:user_id)
  		flash[:notice] = "Thank you for visiting!"
  	end
  	redirect_to root_path
  end

  private 
 	def find_state_id 
 		state_name = params["location"]
 		@state_id ||= State.where('UPPER(name) LIKE UPPER(:name)', name: "%#{state_name}%").first.id
 	end
end
