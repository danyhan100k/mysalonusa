$(document).on('turbolinks:load', function() {

  var swiper = new Swiper('.welcome-page-featured-jobs.swiper-container', {
	  pagination: '.swiper-pagination',
	  autoplay: 5000,
	  speed: 400,
	  slidesPerGroup: 2,
	  slidesPerView: 5,
	  slidesPerColumn: 2,
	  paginationClickable: true,
	  spaceBetween: 10,
	  breakpoints: {
      1024: {
        slidesPerView: 4,
        spaceBetween: 10
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 10
      },
      640: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      }
    }
  });

});