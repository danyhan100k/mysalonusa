$(document).on('turbolinks:load', function(){

	var width = $(window).width();
  if (width > 700) {
    businessOnSaleSwiper(4)
  } else {
    businessOnSaleSwiper(1)
  }

  $('.business-sale-swiper-container').on('click', function(){
  	window.location.pathname = "/business_on_sales";
  });
  function businessOnSaleSwiper(n) {
    var businessSaleSwiper = new Swiper('.business-sale-swiper-container', {
      pagination: '.business-sale-swiper-pagination',
        slidesPerView: n,
        slidesPerColumn: 2,
        paginationClickable: true,
        spaceBetween: 22,
        autoplay: 6000
    });
  };

})