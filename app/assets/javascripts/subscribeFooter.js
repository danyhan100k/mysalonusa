//subscribe button on footer

$(document).on('turbolinks:load', function() {

	$('.subscribe-button').on('click', function() {
		var email = $('#subscribe-footer').val(); 
		if (!email) { return createModal("Need Email",'Please Provide Your Email')}
		if (!validateEmail(email)) { return createModal("Need Valid Format", "Please Provide Valid Email: ex) subscribe@gmail.com") }
		if (email) {
			subscribeEmail(email);
		};
	});

	function subscribeEmail(email) {
		var urlEncoded = $.param({
			email: email
		});

		$.ajax('/subscriptions?' + urlEncoded, {
			method: 'POST'
		})
		.then(
			function success(data) {
				if (data.errors) {
					return(
						createModal(
							"Error",
							data.errors
						)
					);
				};
				createModal(
					"Thank you for subscribing!",
					"We will update you on new Job & Business posts!"
				);
				$('#subscribe-footer').val('');
			}, 
			function fail(data, status) {
				alert('Please Refresh and Try Again');
			}
		);

	};

	function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
	};

	function createModal(title,msg){
		var modal = 
			"<div class='modal fade' tabindex='-1' role='dialog'>" +
			  "<div class='modal-dialog' role='document'>" +
			    "<div class='modal-content'>" +
			      "<div class='modal-header'>" +
			        "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" +
			        "<h4 class='modal-title'>" + title + 
			        "<i class='alarm outline icon'></i>" + 
			        "</h4>" +
			      "</div>" +
			      "<div class='modal-body'>" +
			        "<p>" + 
			        msg +
			        "</p>" +
			      "</div>" +
			      "<div class='modal-footer'>" +
			      	"<button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>" +
			      "</div>" +
			    "</div>" +
			  "</div>" +
			"</div>";
		$(modal).modal();
	};


});