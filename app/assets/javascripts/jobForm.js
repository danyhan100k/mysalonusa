"use strict";

$(document).on('turbolinks:load', function() {
  //below is only_job_form
  $("form.only-job-form").on('submit', function(e){
    if (check_language_checkboxes()) {
      $('.only-job-form input[type="submit"]').prop("disabled", true);
      $("form.only-job-form").submit();
    } else {
      e.preventDefault();
    };
  });

  // below is all company_location_and_job_form
	var step1Form = $('.step-1-job-form');
	var step2Form = $('.step-2-job-form');
  var submitButton = step2Form.find('.create-job-button');
	step2Form.hide();

	$('.job-form-next-button').on('click', function(ev) {
		ev.preventDefault();
    if (checkStep1Form()) {
			step1Form.hide();
			step2Form.show();	
		}
	});

	$('.job-form-back-button').on('click', function(ev) {
		ev.preventDefault();
		step2Form.hide();
		step1Form.show();
	});

	$('.user-companies-select').on('change', function() {
		var defaultValue = '';
		var selectedValue= $(this).val();
		var companyNameInput = $('input.company_name_input');

		if (selectedValue !== defaultValue) {
			companyNameInput.prop("disabled", true);
			$('.new-company-form-wrapper').slideUp();
		} else {
			companyNameInput.removeProp("disabled");
			$('.new-company-form-wrapper').slideDown();
		}
	});

  submitButton.click(function (ev) {
    if (!checkStep2Form()) {
      ev.preventDefault();
    } else {
      $('.create-job-button').prop("disabled", true);
      $('.new_job').submit();
    };
  });

	function checkStep1Form() {
		var companySelect = $('select.user-companies-select');
		var companyNameInput = $('input.company_name_input');
    
		var result = true;

    //if companySelect is not present, we need to add this attr to company_name_input
    //so that validateRequiredForStep will be run to remove validations
    if (companySelect.length == 0) {
      $('#job_company_location_attributes_company_attributes_name')
      .attr('data-validate-required', "Please enter company name")
    }

    if ( // if CompanySelect exists and is empty and CompanyNameInput is empty
        companySelect.length > 0 &&
        isEmpty(companySelect) &&
        !validateRequired(
          companyNameInput,
          "Please enter company name"
        )
      ) {
      result = false;
    }

    if ( // if CompanySelect does not exist && company_name_input is empty
        companySelect.length == 0 && 
        isEmpty(companyNameInput) && 
        !validateRequired(companyNameInput, "Please enter company name") 
        ) {
      result = false
    }

    if (!validateRequiredForStep(step1Form)) {
      result = false;
    }


		return result;
	}

  function checkStep2Form() {
    return validateRequiredForStep(step2Form);
  }

  function validateRequiredForStep(step) {
    var result = true;
    step.find("[data-validate-required]").each(function(index, el) {
      el = $(el);
      if (!validateRequired(el, el.data("validate-required"))) {
        result = false
      }
    });
    if (result == false) {return result;}
    if (step.find(".post-job-which-language-question-wrapper").length) {
      result = check_language_checkboxes();
    };
    return result;
  };

  var errorBoxTemplate =
    '<div class="error-box ui pointing red basic label hidden"></div>';
  function validateRequired(input, message) {
    var errorBox = input.siblings(".error-box");
    if (errorBox.length === 0) {
      errorBox = $(errorBoxTemplate);
      errorBox.insertAfter(input);
    }

    if (isEmpty(input)) {
      errorBox.removeClass("hidden");
      errorBox.html(message);
      return false;
    } else {
      errorBox.addClass("hidden");
      return true;
    }
  }

  function isEmpty(input) {
    return input.val().length === 0;
  }

  function check_language_checkboxes() {
    var errorMessage = "Please choose language";
    var checked = $("input.language-checkbox:checked").length;
    if (!checked) {
      $(".checkbox-container .error-box").removeClass('hidden');
      console.log('returning false')
      return false;
    } else {
      $(".checkbox-container .error-box").addClass('hidden');
      console.log('returning true')
      return true;
    };
  };

});
