$(document).on("turbolinks:before-visit", function() {
  $('.job-company-search').typeahead('destroy');
  $('.address-search').typeahead('destroy');
});

$(document).on("turbolinks:load", function(){
	autoCompleteWhere();
	autoCompleteWhat();

	function autoCompleteWhat() {
		var whats = new Bloodhound({
	    datumTokenizer: Bloodhound.tokenizers.whitespace,
	    queryTokenizer: Bloodhound.tokenizers.whitespace,
	    remote: {
	      url: '/api/jobs/autocomplete/what?searchValue=%QUERY',
	      wildcard: '%QUERY',
	      rateLimitBy: 'throttle',
	      rateLimitWait: 100,
	    }
	  });

	  $('.job-company-search').typeahead(null, {
	    source: whats,
	    limit: 15
	  });
	};

	function autoCompleteWhere() {
		var locations = new Bloodhound({
	    datumTokenizer: Bloodhound.tokenizers.whitespace,
	    queryTokenizer: Bloodhound.tokenizers.whitespace,
	    remote: {
	      url: '/api/jobs/autocomplete/where?searchValue=%QUERY',
	      wildcard: '%QUERY',
	      rateLimitBy: 'throttle',
	      rateLimitWait: 100,
	    }
	  });

	  $('.address-search').typeahead(null, {
	    source: locations,
	    limit: 15
	  });
	};

  $("#search-form")
    .on("ajax:before", showSpinner)
    .on("ajax:success", loadJobs);

  function showSpinner() {
		$("#search-form button").prop('disabled', true); 
		$('.jobs-header-container').html('');
		$('.jobs-wrapper').html('');
		$('.loader').css('visibility', 'visible');
  }

	function loadJobs(_, html) {
		$('.loader').css('visibility', 'hidden');
		$('.job-search-button').prop('disabled', false)
		$('#jobs-list-wrapper').html(html);
		rebindJobsForModal();
	}

  // TODO: Replace this with HTML form
	function triggerApiJobs(searchValue) {
		document.location.href = '/api/jobs?jobCompanySearch=' + searchValue;
		$('.job-company-search').val(searchValue);
	};

	$('.welcome.jobs-side-menu a.category-item').on('click', function(event){
		event.preventDefault();
		triggerApiJobs($(this).find('span.item-name').text());
	});

	$('.api_jobs.jobs-side-menu a.category-item').on('click', function(event) {
		event.preventDefault();
		triggerApiJobs($(this).find('span.item-name').text());
	});

	function rebindJobsForModal() {
		$('.job-title-link').on('click', function() {
			var jobId = $(this).attr('data-job-id');

			$.ajax({
	      url: '/api/jobs/search_by_id?id=' + jobId,
	      dataType: 'json',
	      type: 'GET',
	      error: function() {
	console.log('Error found after making request for job modal: ', arguments);
	      },
	      success: function(job) {
					var modal = createModal(job);
					$(modal).modal();
	      },
	  	});
		}); //.job-title-link

		function createModal(job) {
			var createdAt = new Date(job.created_at_utc).toLocaleString();
			if ($(window).width() >= 992) {
				createdAt = jQuery.timeago(createdAt);
			};
			var jobType = job.job_type;
			var contactEmail = job.contact_email;
			var companyName = job.company.name;
			var address = job.address.address_line_1 + job.address.address_line_2;
			var cityStateZip = (job.address.city.name ? job.address.city.name + " " : "") + 
												 job.address.city.state.name + " " + 
												 (job.address.city.zip ? job.address.city.zip : "");

			var modal = 
				"<div class='modal fade' tabindex='-1' role='dialog'>" +
				  "<div class='modal-dialog' role='document'>" +
				    "<div class='modal-content'>" +
				      "<div class='modal-header'>" +
				        "<button type='button' class='close' data-dismiss='modal' aria-label='Close'>" +
				        	"<span aria-hidden='true'>&times;</span>" +
				        "</button>" +
				        "<h3 class='modal-title'>" + job.title + "</h3>" +
				      "</div>" +
				      "<div class='modal-body'>" +
				        "<p class='job-modal-description'>" + job.description + 
				        "</p>" +
				      "</div>" +
				       "<div class='list-group'>" + 
				        "<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Contact Email: </div>"+ "<a href='mailto:" + contactEmail + "'>" + contactEmail + "</a>" + "</button>" + 
				        "<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Salon Name: </div>"+ companyName + "</button>" +
				        "<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Job Type: </div>"+ jobType + "</button>" +
				        "<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Address: </div>"+ address + "</button>" + 
	 							"<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>City & State: </div>"+ cityStateZip + "</button>" + 
	 							"<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Posted Date: </div>"+ createdAt + "</button>" + 
				      "</div>" + 
				      "<div class='modal-footer' style='border-top: none;'>" +
				        "<button type='button' class='btn btn-primary' data-dismiss='modal' style='background-color: #ee5c8e; border-color: #ee5c8e;'>Close</button>" +
				      "</div>" +
				    "</div>" +
				  "</div>" +
				"</div>";
				return modal;
		}; //createModal
	}; //rebindJobsForModal
});//document
