$(document).on("turbolinks:load", function() {
// Extra small devices Phones (<768px)
// Small devices Tablets (≥768px)
// Medium devices Desktops (≥992px)
// Large devices Desktops (≥1200px)
//
	if ($(window).width() < 768) {
		changeWelcomePageBusinessOnSaleBanner();
		changeBusinessOnSaleIndexPageBanner();
	};
	//
	function changeWelcomePageBusinessOnSaleBanner() {
		var smallBanner = "https://s3.us-east-2.amazonaws.com/mysalonusa/uploads/banners/small+banners/BUTTOM_BANNER_mobile_2.jpg";
		$('#welcome-page-business-on-sale-banner img').attr("src", smallBanner);
	};

	function changeBusinessOnSaleIndexPageBanner() {
		var smallBanner = "https://s3.us-east-2.amazonaws.com/mysalonusa/uploads/banners/small+banners/BUTTOM_BANNER_mobile_1.jpg";
		$('#business-on-sale-index-page-banner img').attr("src", smallBanner);
	};

});