$(document).on('turbolinks:load', function(){
	$('.flash-message-x').on('click', function closeBox(){
		var flashBox = $('.flash-message-x').closest('.message.flash')
		flashBox.hide();
	});

	$('.description').each(function() {
		var theContent = $(this).html();
		var shortened = theContent.substr(0,150) + '...';
		$(this).html(shortened);
	});

	const width = $(window).width()
	if (width <= 991) {
		$('.react-nav-menu').hide();
	}
});