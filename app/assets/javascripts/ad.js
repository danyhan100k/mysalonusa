$(document).on("turbolinks:load", function() {
	var destination = 327; //kyocharo-ad
	var $window = $(window);

	document.addEventListener("scroll", fixAdIfDesktop, true);
  $(document).one("turbolinks:visit", function() {
    document.removeEventListener("scroll", fixAdIfDesktop, true);
  });

  function fixAdIfDesktop(event) {
		if (mediumDevice() && getDocumentScrollTop() >= destination) {
			$("#kyocharo-ad").css({
				position: "fixed",
				top: "12em"
			});
		} else {
			$("#kyocharo-ad").removeAttr("style");
		};
	}
 	
	function getDocumentScrollTop() {
//http://stackoverflow.com/questions/22173454/jquery-scrolltop-returns-0-for-all-elements
		var l = $('*').length;
		for(var i = 0; i < l; i++) {
		    var elem = $('*:eq(' + i + ')');
		    if(elem.scrollTop() > 0) {
		        return elem.scrollTop();
		    }
		}
	};

	function mediumDevice() {
		//Medium devices Desktops (≥992px)
		return $(window).width() >= 992
	};
});
