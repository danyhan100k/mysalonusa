//= require webpack-bundle

// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap-sprockets
//= require jquery.turbolinks
//= require jquery_ujs
//= require material
//= require swiper
//= require globals
//= require remodal.min
//= require popupModal
//= require lock
//= require choosePlan
//= require typehead
//= require jobs_search
//= require timeAgo
//= require jobForm
//= require businessOnSaleSwiper
//= require extra
//= require jobModal
//= require subscribeFooter
//= require popupModal2
//= require mediaQuery
//= require featuredJobs(welcome_page)
//= require ad
//= require turbolinks

// not used yet require_tree 

// document.addEventListener('page:change', function() {
//   componentHandler.upgradeDom();
// });

document.addEventListener('turbolinks:load', function() {
  componentHandler.upgradeDom();
});

$(document).on('turbolinks:load', function triggerAnalytics() {
  if (window.clicky) {
  	clicky.log( document.location.pathname + document.location.search, document.title, 'pageview' );
  };
});
