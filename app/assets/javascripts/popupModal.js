$(document).on("turbolinks:load", function() {
  //this is popUpmodal for both language + state in initial landing!
  var $welcomeModal = $('#initial-settings');
  var modalHeader = $('.initial-settings-modal').find('.modal-header');
  var step1 = $welcomeModal.find('#step-1');
  var step2 = $welcomeModal.find('#step-2');

  step2.hide();
  step1.find('input[name=location]').on('click', function moveToStep2() {
    modalHeader.hide();
    step1.hide();
    step2.show();
  });
  step2.find('input[name=locale]').on('click', function submitForm() {
    $welcomeModal.find('form').submit();
    $('#initial-settings').html("<div class='ui active dimmer' style='height: 77px;'><div class='ui medium text loader'>Loading</div></div>");
  });
  step2.find('#back').on('click', function backToStep1() {
    $('input[name=location]').attr('checked', false); 
    step2.hide();
    modalHeader.show();
    step1.show();
  })
  if (!getCookie('locale') || !getCookie('location') || !getCookie('state_name')) {
    msieversion();
  };

  //checks if user is using internet explorer
  function msieversion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
    {
        // alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
        $('.initial-settings-modal.modal').modal({
          backdrop: false,
          keyboard: false,
        });
        $('.ie-detected.modal').modal();  
    }
    else  // If another browser, return 0
    {
        $('.initial-settings-modal.modal').modal({
          backdrop: false,
          keyboard: false,
        });
    }
    return false;
  };
  
});
