$(document).on('turbolinks:load', function() {

	$('.job-title-link').on('click', function() {
		var jobId = $(this).attr('data-job-id');
		var url = '/api/jobs/search_by_id?id=' + jobId;
		ajaxJob(url);
	}); //.job-title-link

	function ajaxJob(url) {
		$.ajax({
      url: url,
      dataType: 'json',
      type: 'GET',
      error: function() {
console.log('Error found after making request for job modal: ', arguments);
      },
      success: function(job) {
				var modal = createModal(job);
				$(modal).modal();
      },
  	});
	};

	function createModal(job) {
		var createdAt = new Date(job.created_at_utc.replace(/-/g, "/")).toLocaleString();
		if ($(window).width() >= 992) {
			createdAt = jQuery.timeago(createdAt);
		};
		var jobType = job.job_type;
		var jobTitle = job.job_title || "Unknown";
		var contactEmail = job.contact_email;
		var companyName = job.company.name;
		var address = job.address.address_line_1 + job.address.address_line_2;
		var cityStateZip = (job.address.city.name ? job.address.city.name + " " : "") + 
											 job.address.city.state.name + " " + 
											 (job.address.city.zip ? job.address.city.zip : "");

		var modal = 
			"<div class='modal fade' tabindex='-1' role='dialog'>" +
			  "<div class='modal-dialog' role='document'>" +
			    "<div class='modal-content'>" +
			      "<div class='modal-header'>" +
			        "<button type='button' class='close' data-dismiss='modal' aria-label='Close'>" +
			        	"<span aria-hidden='true'>&times;</span>" +
			        "</button>" +
			        "<h3 class='modal-title'>" + job.title + "</h3>" +
			      "</div>" +
			      "<div class='modal-body'>" +
			        "<p class='job-modal-description'>" + job.description + 
			        "</p>" +
			      "</div>" +
			       "<div class='list-group'>" +  
			        "<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Contact Email: </div>"+ "<a href='mailto:" + contactEmail + "'>" + contactEmail + "</a>" + "</button>" + 
			        "<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Salon Name: </div>"+ companyName + "</button>" +
			        "<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Job Title: </div>"+ jobTitle + "</button>" +
			        "<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Job Type: </div>"+ jobType + "</button>" +
			        "<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Address: </div>"+ address + "</button>" + 
 							"<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>City & State: </div>"+ cityStateZip + "</button>" + 
 							"<button type='button' class='list-group-item'><div class='ui label teal job-modal-label'>Posted Date: </div>"+ createdAt + "</button>" + 
			      "</div>" + 
			      "<div class='modal-footer' style='border-top: none;'>" +
			        "<button type='button' class='btn btn-primary' data-dismiss='modal' style='background-color: #ee5c8e; border-color: #ee5c8e;'>Close</button>" +
			      "</div>" +
			    "</div>" +
			  "</div>" +
			"</div>";
			return modal;
	}; //createModal
});