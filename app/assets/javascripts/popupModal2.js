$(document).on("turbolinks:load", function() {
	//popups for changing state and language
	var $changeLanguageModal = $('.choose-language-modal.modal');
  var $changeStateModal = $('.choose-state-modal.modal');

	$('.change_language_link').on('click', function(ev) {
		ev.preventDefault();
		$changeLanguageModal.modal();
		$('#choose-language-modal input').on('click', function(ev){
			var form = $('#choose-language-modal form');
			$(form).submit();
			createLoader('#choose-language-modal');
		});
	});

	$('.change_state_link').on('click', function(ev) {
		ev.preventDefault();
		$changeStateModal.modal();
		$('#choose-state-modal input').on('click', function() {
			var form = $('#choose-state-modal form');
			$(form).submit();
			createLoader('#choose-state-modal');
		});
	});

	function createLoader(selector) {
		var loader = "<div class='ui active dimmer' style='height: 70px;'><div class='ui medium text loader'>Loading</div></div>";
		$(selector).html(loader);
	}

}); 