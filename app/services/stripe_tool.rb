module StripeTool
	class << self
		def create_membership(email: email, stripe_token: stripe_token, plan: plan)
			Stripe::Customer.create(
				email: email,
				source: stripe_token,
				plan: plan
			)
		end
		def create_customer(email: email, stripe_token: stripe_token)
			Stripe::Customer.create(
				email: email,
				source: stripe_token
			)
		end
		def create_charge(customer_id: customer_id, amount: amount, description: description)
			Stripe::Charge.create(
				customer: customer_id,
				amount: amount,
				description: description,
				currency: 'usd'
			)
		end
		def create_payment(amount_to_be_charged:, stripe_email:, stripe_token:, description:, user:, plan:, type:)
			customer = StripeTool.create_customer(email: stripe_email, 
																							stripe_token: stripe_token)
			charge = StripeTool.create_charge(customer_id: customer.id,
																					amount: amount_to_be_charged,
																					description: description)
			user.update_status_to(User.statuses['paid']) if user.unpaid?
			if type == 'business_on_sale'
			 	return BusinessOnSalePayment.create_and_update_token_for_user(user, plan, customer.id, charge.id, description)
			elsif type == 'job'
				return Payment.create_and_update_token_for_user(user, plan, customer.id, charge.id, description)
			end
		end
	end
end