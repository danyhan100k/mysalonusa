class JobsSearchAdapter
	attr_accessor :params, :state, :default_search
	def initialize(params)
		self.params = params
	end
  # TODO: Find a way to include nested records AND keep the pagination's methods entry_name and total_count
	def search #params["searchValue"].try :[], "jobCompanySearch"
#for Test
    search_terms << job_company_terms
    search_terms << address_terms
    search_terms << main_page_search_info
    search_terms << main_page_search_location

    terms = search_terms.select(&:present?)
    # fail "Location isn't defined" if params[:location].blank?
    terms << params[:location] if terms.blank?

    Job.search(
      terms.join(' '),
      page: page,
      per_page: per_page,
      operator: 'and',
      order: { created_at: :desc}, #TODO: Changing implementation to load: false
      fields: [
               :title, :job_type, 'address.address_line_1', 
               'address.address_line_2', 'address.city.name', 
               'address.city.zip', 'address.city.state.name', 
               'address.city.state.abbreviation', 'languages.languages'
              ]
    )
  end

  def default_search_main_page
  	searched = Job.search("*", 
						  			where: { 'company_location.address.city.state.abbreviation': params[:state][:abbreviation] },
						  			limit: 25, 
						  			order: { created_at: :desc }
						  	).records.includes(INCLUDES)
  end

	def as_json(*)
    search
      .records
      .includes(INCLUDES)
      as_json(JSON_SCHEMA)
	end

	JSON_SCHEMA = {
		include: {
			company_location: {
        include: {
          company: { only: [:name] },
          address: {
            include: {
              city: {
                include: {
                  state: { only: [:name, :abbreviation] }
                },
                only: [:name, :zip]
              }
            },
            only: [:address_line_1, :address_line_2]
          },
        },
        only: []
      }
    },
    only: [:id, :title, :description, :job_type, :views, :created_at_reformat]
  }

  private
  def page 
    params[:page].to_i
  end

  def per_page 
    params[:length] || 25
  end

  def address_terms
    params["addressSearch"]
  end

  def job_company_terms
    params["jobCompanySearch"]
  end

  def search_terms
    @search_terms ||= Set.new
  end

  def main_page_search_info
    params['main_page_search_info']
  end

  def main_page_search_location
      params['main_page_search_location']
  end

  INCLUDES = [ 
    :company_location => [
      :company,
      {
        address: {
          city: :state
        }
      }
    ] 
  ]
end
