ActiveAdmin.register UsedItem do
	permit_params :user_id, :city_id, :title, :description, 
	:views, :contact_email, :price_cents, :price_currency, :images


	form do |f|
		f.inputs do
			f.input :user
			f.input :city
	    f.input :title
	    f.input :description
	    f.input :contact_email
	    f.input :price_cents
	    f.input :price_currency
	    f.input :views
	  	end
	  f.actions
	end

end
