ActiveAdmin.register BusinessOnSale do
	permit_params :title, :description, 
	:views, :contact_email, :price_cents, :price_currency, :images

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

	form do |f|
		f.inputs do
	    f.input :id
	    f.input :title
	    f.input :description
	    f.input :contact_email
	    f.input :price_cents
	    f.input :price_currency
	    f.input :views
	  	end
    f.actions
  end



end
